<?php
include "layout/headerIndex.php";
require_once "API/koneksi.php";
require "includes/func.inc.php";
$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM PENGUMUMAN P WHERE P.ID_PENGUMUMAN = :v1 ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}
?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Detail Data Pengumuman</h6>
                </div>
                <div class="container-fluid py-4">
                    <form>
                        <div class="row">
                            <div class="form-group">
                                <h6>File Pengumuman</h6>
                                <div class="mb-2 d-flex flex-column">
                                    <iframe src="file_pengumuman/<?=$hasil['FILE_PENGUMUMAN']?>" width="1200" height="600"></iframe>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-info" href="informasi.php" role="button" style="float: right">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "layout/footerIndex.php"; ?>