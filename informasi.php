<?php
include "layout/headerIndex.php";
require_once "API/koneksi.php";
require "includes/func.inc.php";
require "includes/config.inc.php";
require_once "API/timKewirausahaan.php";
$hasil = query_viewTimKewirausahaan();
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "API/pengumuman.php";
$hasilPengumuman = query_viewDataPengumuman();
oci_fetch_all($hasilPengumuman, $rowsPengumuman, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

//require_once "API/jumlahTim.php";
//$hasilJumlahTim = query_countJumlahTim();
//oci_fetch_all($hasilJumlahTim, $rowsJumlahTim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
//$jumlah_tim = intval($rowsJumlahTim[0]['JUMLAH']);
//
//require_once "API/jumlahLomba.php";
//$hasilJumlahLomba = query_countJumlahLomba();
//oci_fetch_all($hasilJumlahLomba, $rowsJumlahLomba, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
//$jumlah_lomba = intval($rowsJumlahLomba[0]['JUMLAHLOMBA']);
//
//require_once "API/jumlahPelatihan.php";
//$hasilJumlahPelatihan = query_countJumlahPelatihan();
//oci_fetch_all($hasilJumlahPelatihan, $rowsJumlahPelatihan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
//$jumlah_pelatihan = intval($rowsJumlahPelatihan[0]['JUMLAHPEL']);

require_once "API/jumlahJurusan.php";

$hasilJumlahTotal = query_JumlahTotal();
oci_fetch_all($hasilJumlahTotal, $rowsJumlahTotal, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Total = count($rowsJumlahTotal);

$hasilJumlahIT = query_JumlahAnggotaIT();
oci_fetch_all($hasilJumlahIT, $rowsJumlahIT, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_IT = count($rowsJumlahIT);

$hasilJumlahElka = query_JumlahAnggotaElka();
oci_fetch_all($hasilJumlahElka, $rowsJumlahElka, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Elka = count($rowsJumlahElka);

$hasilJumlahTelkom = query_JumlahAnggotaTelkom();
oci_fetch_all($hasilJumlahTelkom, $rowsJumlahTelkom, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Telkom = count($rowsJumlahTelkom);

$hasilJumlahElin = query_JumlahAnggotaElin();
oci_fetch_all($hasilJumlahElin, $rowsJumlahElin, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Elin = count($rowsJumlahElin);

$hasilJumlahMeka = query_JumlahAnggotaMeka();
oci_fetch_all($hasilJumlahMeka, $rowsJumlahMeka, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Meka = count($rowsJumlahMeka);

$hasilJumlahTekkom = query_JumlahAnggotaTekkom();
oci_fetch_all($hasilJumlahTekkom, $rowsJumlahTekkom, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Tekkom = count($rowsJumlahTekkom);

$hasilJumlahMMB = query_JumlahAnggotaMMB();
oci_fetch_all($hasilJumlahMMB, $rowsJumlahMMB, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_MMB = count($rowsJumlahMMB);

$hasilJumlahSPE = query_JumlahAnggotaSPE();
oci_fetch_all($hasilJumlahSPE, $rowsJumlahSPE, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_SPE = count($rowsJumlahSPE);

$hasilJumlahGame = query_JumlahAnggotaGame();
oci_fetch_all($hasilJumlahGame, $rowsJumlahGame, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_Game = count($rowsJumlahGame);

$hasilJumlahTRI = query_JumlahAnggotaTRI();
oci_fetch_all($hasilJumlahTRI, $rowsJumlahTRI, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_TRI = count($rowsJumlahTRI);

$hasilJumlahTRM = query_JumlahAnggotaTRM();
oci_fetch_all($hasilJumlahTRM, $rowsJumlahTRM, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_TRM = count($rowsJumlahTRM);

$hasilJumlahDataSains = query_JumlahAnggotaDataSains();
oci_fetch_all($hasilJumlahDataSains, $rowsJumlahDataSains, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
$jumlah_DataSains = count($rowsJumlahDataSains);
?>

<!--<div class="container-fluid py-4">-->
<!--    <div class="row">-->
<!--        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">-->
<!--            <div class="card">-->
<!--                <div class="card-body p-3">-->
<!--                    <div class="row">-->
<!--                        <div class="col-8">-->
<!--                            <div class="numbers">-->
<!--                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Tim UMKM</p>-->
<!--                                <h5 class="font-weight-bolder mb-0">-->
<!--                                    --><?php //echo $jumlah_tim?>
<!--                                    <span class="text-success text-sm font-weight-bolder"></span>-->
<!--                                </h5>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-4 text-end">-->
<!--                            <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">-->
<!--                                <i class="fas fa-home text-lg opacity-10" aria-hidden="true"></i>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">-->
<!--            <div class="card">-->
<!--                <div class="card-body p-3">-->
<!--                    <div class="row">-->
<!--                        <div class="col-8">-->
<!--                            <div class="numbers">-->
<!--                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Pelatihan</p>-->
<!--                                <h5 class="font-weight-bolder mb-0">-->
<!--                                    --><?php //echo $jumlah_pelatihan?>
<!--                                    <span class="text-success text-sm font-weight-bolder"></span>-->
<!--                                </h5>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-4 text-end">-->
<!--                            <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">-->
<!--                                <i class="fas fa-chalkboard-teacher text-lg opacity-10" aria-hidden="true"></i>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">-->
<!--            <div class="card">-->
<!--                <div class="card-body p-3">-->
<!--                    <div class="row">-->
<!--                        <div class="col-8">-->
<!--                            <div class="numbers">-->
<!--                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Kompetisi</p>-->
<!--                                <h5 class="font-weight-bolder mb-0">-->
<!--                                    --><?php //echo $jumlah_lomba?>
<!--                                    <span class="text-danger text-sm font-weight-bolder"></span>-->
<!--                                </h5>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-4 text-end">-->
<!--                            <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">-->
<!--                                <i class="fas fa-award text-lg opacity-10" aria-hidden="true"></i>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <br>-->
    <div class="row my-4">
        <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="text-end">
                            <a href="index.php"><i class="fas fa-window-close me-2 fs-5" style="color: black"></i></a>
                        </div>
                        <div class="col-lg-6 col-7">
                            <h6 style="margin-left: 20px">Rekap Jumlah Mahasiswa Per Jurusan</h6>
                        </div>
                    </div>
                        <div class="card-body">
                            <p class="d-flex flex-column">
                                <span class="text-bold text-lg">Total <?php echo $jumlah_Total?></span>
                            </p>
                            <div class="row">
                            <div class="col-6">
                            <div class="progress-group">
                                Teknik Elektronika
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Elka ?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-warning" style="width: <?php echo ($jumlah_Elka/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>
                            <!-- /.progress-group -->

                            <div class="progress-group">
                                Teknik Telekomunikasi
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Telkom ?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-success" style=" width: <?php echo ($jumlah_Telkom/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknik Elektro Industri
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Elin ?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_Elin/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknik Informatika
                                <span class="" style="font-weight:bold; float: right"><?php echo $jumlah_IT?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-info" style="width: <?php echo ($jumlah_IT/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknik Mekatronika
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Meka?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar" style="width: <?php echo ($jumlah_Meka/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknik Komputer
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Tekkom?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_Tekkom/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>
                        </div>
                    <!-- /.card -->
                    <div class="col-6">
                            <div class="progress-group">
                                Multimedia Broadcasting
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_MMB?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-warning" style="width: <?php echo ($jumlah_MMB/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>
                            <!-- /.progress-group -->

                            <div class="progress-group">
                                Sistem Pembangkit Energi
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_SPE?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-success" style=" width: <?php echo ($jumlah_SPE/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknologi Game
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Game?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_Game/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknologi Rekayasa Internet
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_TRI?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-info" style="width: <?php echo ($jumlah_TRI/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Teknologi Rekayasa Multimedia
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_TRM?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar" style="width: <?php echo ($jumlah_TRM/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>

                            <div class="progress-group">
                                Sains Data Terapan
                                <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_DataSains?></span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_DataSains/$jumlah_Total)*100; ?>%"></div>
                                </div>
                            </div> <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid py-4">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="row">
                    <div class="card-header pb-0 px-3 pt-4" style="margin-left: 30px">
                        <h6 class="mb-0">Pengumuman</h6>
                    </div>
                </div>
                <div class="card-body pt-4 p-3">
                    <div class="table-responsive px-3">
                        <table class="table align-items-center mb-0" id="myTable">
                            <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Judul</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php  foreach ($rowsPengumuman as $hasil) {?>
                                <tr>
                                    <td>
                                        <div class="d-flex px-3 py-1">
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm"><?php echo $hasil['NAMA_PENGUMUMAN']; ?></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="align-middle text-center">
                                        <div class="ms-auto">
                                            <a class="btn btn-link text-info text-gradient px-3 mb-0" href="home.php?halaman=detail-pengumuman-index&id=<?php echo $hasil['ID_PENGUMUMAN'];?>"><i class="far fa-eye me-2"></i>Detail</a>
                                            </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include "layout/footerIndex.php"; ?>