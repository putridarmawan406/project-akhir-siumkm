<?php
require_once "koneksi.php";

function query_viewDataMahasiswa()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT * FROM MAHASISWA M 
		JOIN KELAS K ON M.KELAS=K.NOMOR 
		JOIN JURUSAN J ON J.NOMOR=K.JURUSAN');

    oci_execute($parse);
    return $parse;
}
?>
