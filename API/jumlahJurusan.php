<?php
require_once "koneksi.php";

function query_JumlahTotal()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaElka()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 1 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaTelkom()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 2 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaElin()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 3 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaIT()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 4 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaMeka()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 5 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaTekkom()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 6 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaMMB()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 7 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaSPE()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 8 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaGame()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 9 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaTRI()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 10 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaTRM()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 11 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}

function query_JumlahAnggotaDataSains()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA FROM JURUSAN J JOIN ANGGOTA_TIM A ON A.ID_JURUSAN=J.NOMOR 
                                      WHERE J.NOMOR = 12 GROUP BY A.ANGGOTA');

    oci_execute($parse);
    return $parse;
}
?>
