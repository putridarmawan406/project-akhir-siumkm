<?php
require_once "koneksi.php";

function query_viewLaporan()
{
		global $conn;
		$parse = oci_parse($conn, "SELECT * FROM LAPORAN L JOIN STATUS_VALIDASI SV ON SV.ID_STATUS = L.STATUS_LAPORAN");

		oci_execute($parse);
    	return $parse;
}

function query_viewTimBimbingan(){
	global $conn;
	$idDosen = $_SESSION["dosbim"];
	$parse = oci_parse($conn, "SELECT * FROM LAPORAN L  JOIN TIM_KEWIRAUSAHAAN T ON L.ID_TIM = T.ID_TIM 
									  JOIN STATUS_VALIDASI SV ON SV.ID_STATUS = L.STATUS_LAPORAN WHERE T.DOSBIM = '$idDosen' ");

	oci_execute($parse);
	return $parse;
}
function query_viewLaporanTim(){
	global $conn;
	$idTim = $_SESSION["id"];
	$parse = oci_parse($conn, "SELECT * FROM LAPORAN L  JOIN TIM_KEWIRAUSAHAAN T ON L.ID_TIM = T.ID_TIM 
									  JOIN STATUS_VALIDASI SV ON SV.ID_STATUS = L.STATUS_LAPORAN WHERE T.ID_TIM = '$idTim' ");

	oci_execute($parse);
	return $parse;
}
?>
