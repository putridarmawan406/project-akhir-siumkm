<?php
require_once "koneksi.php";

function query_countJumlahPMW()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT COUNT(*) AS jumlahPMW FROM TIM_KEWIRAUSAHAAN WHERE KATEGORI=2');

    oci_execute($parse);
    return $parse;
}

function query_countJumlahPWMV()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT COUNT(*) AS jumlahPWMV FROM TIM_KEWIRAUSAHAAN WHERE KATEGORI=1');

    oci_execute($parse);
    return $parse;
}

function query_countJumlahPMWPWMV()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT COUNT(*) AS jumlahPMWPWMV FROM TIM_KEWIRAUSAHAAN WHERE KATEGORI=7');

    oci_execute($parse);
    return $parse;
}

function query_countJumlahPKM_K()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT COUNT(*) AS jumlahPKM_K FROM TIM_KEWIRAUSAHAAN WHERE KATEGORI=3');

    oci_execute($parse);
    return $parse;
}

function query_countJumlahKBMI()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT COUNT(*) AS jumlahKBMI FROM TIM_KEWIRAUSAHAAN WHERE KATEGORI=6');

    oci_execute($parse);
    return $parse;
}
?>
