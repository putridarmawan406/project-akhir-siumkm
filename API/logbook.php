<?php
require_once "koneksi.php";

function query_viewLogbook()
{
	global $conn;
	$parse = oci_parse($conn, "SELECT * FROM LOGBOOK L JOIN STATUS_VALIDASI SV ON SV.ID_STATUS = L.STATUS_LOGBOOK");

	oci_execute($parse);
	return $parse;
}

function query_viewLogbookTimBimbingan(){
	global $conn;
	$idDosen = $_SESSION["dosbim"];
	$parse = oci_parse($conn, "SELECT * FROM LOGBOOK L  JOIN TIM_KEWIRAUSAHAAN T ON L.ID_TIM = T.ID_TIM 
                                      JOIN STATUS_VALIDASI SV ON SV.ID_STATUS = L.STATUS_LOGBOOK WHERE T.DOSBIM = '$idDosen' ");

	oci_execute($parse);
	return $parse;
}
function query_viewLogbookTim(){
	global $conn;
	$idTim = $_SESSION["id"];
	$parse = oci_parse($conn, "SELECT * FROM LOGBOOK L  JOIN TIM_KEWIRAUSAHAAN T ON L.ID_TIM = T.ID_TIM 
									  JOIN STATUS_VALIDASI SV ON SV.ID_STATUS = L.STATUS_LOGBOOK WHERE T.ID_TIM = '$idTim' ");

	oci_execute($parse);
	return $parse;
}
?>
