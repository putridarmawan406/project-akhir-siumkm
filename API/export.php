<?php
require_once "koneksi.php";

function query_viewExport1()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT TM.NAMA_TIM, TM.EMAIL , KU.NAMA_KATEGORI , P.NIP , P.NAMA as NAMA_DOSEN
                                    FROM TIM_KEWIRAUSAHAAN TM 
                                    JOIN KATEGORI_UMKM KU ON TM.KATEGORI=KU.ID_KATEGORI 
                                    JOIN PEGAWAI P ON P.NOMOR=TM.DOSBIM
                                    WHERE P.STAFF=4 ORDER BY TM.NAMA_TIM');

    oci_execute($parse);
    return $parse;
}

function query_viewExport2()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT A.ANGGOTA, M.NAMA,  TM.NAMA_TIM , KU.NAMA_KATEGORI , J.JURUSAN_LENGKAP
                                    FROM ANGGOTA_TIM A
                                    JOIN MAHASISWA M ON M.NRP=A.ANGGOTA
                                    JOIN TIM_KEWIRAUSAHAAN TM ON A.ID_TIM = TM.ID_TIM
                                    JOIN KATEGORI_UMKM KU ON KU.ID_KATEGORI = TM.KATEGORI
                                    JOIN KELAS K ON K.NOMOR = M.KELAS
                                    JOIN JURUSAN J ON J.NOMOR = K.JURUSAN ORDER BY NAMA_TIM');

    oci_execute($parse);
    return $parse;
}
?>
