<?php
require_once "koneksi.php";

function query_viewIdTim()
{
    global $conn;
    $parse = oci_parse($conn, 'SELECT * FROM ANGGOTA_TIM A 
        JOIN MAHASISWA M ON M.NRP=A.ANGGOTA
		JOIN KELAS K ON M.KELAS=K.NOMOR 
		JOIN JURUSAN J ON J.NOMOR=K.JURUSAN');

    oci_execute($parse);
    return $parse;}
?>

$sql2 = "SELECT ID_TIM FROM ANGGOTA_TIM WHERE ID_ANGGOTA_TIM=:v1";
$statement = oci_parse($conn, $sql2);
oci_execute($statement);
return $statement;