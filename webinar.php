<?php
include "layout/headerIndex.php";
require_once "API/koneksi.php";
require "includes/func.inc.php";
require "includes/config.inc.php";
require_once "API/pelatihan.php";
$hasilPelatihan = query_viewPelatihan();
oci_fetch_all($hasilPelatihan, $rowsPelatihan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

?>
<!DOCTYPE html>
<html>
<head>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="contents/vendor/aos/aos.css" rel="stylesheet">
    <link href="contents/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="contents/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="contents/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="contents/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="contents/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="contents/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link rel="stylesheet" type="text/css" href="style.css"/>

</head>
<main id="main">
    <section id="list" class="list">
        <div class="container-flex py-4 px-4">
            <div class="row">
                <div class="text-end">
                    <a href="index.php"><i class="fas fa-window-close me-2 fs-5" style="color: black"></i></a>
                </div>
                <?php  foreach ($rowsPelatihan as $hasilPelatihan) {?>
                    <div class="dosen card" style="width:300px">
                        <div style="height: 400px">
                        <img class="card-img-top" src="foto_pelatihan/<?php echo $hasilPelatihan['POSTER_PEL']; ?>" alt="" style="width:100%">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title text-center"><?php echo $hasilPelatihan['NAMA_PEL']; ?></h4>
                            <p class="card-text text-center"><?php echo $hasilPelatihan['WAKTU_PEL']; ?></p>
<!--                            <div class="text-center mb-2">-->
<!--                                <a class="btn btn-primary px-3 mb-0" href="--><?php //echo $hasilLomba['LINK_LOMBA']; ?><!--">Detail</a>-->
<!--                            </div>-->
                            <!--                        <a class="btn btn-link text-dark px-3 mb-0" href="--><?php //echo $hasilLomba['LINK_LOMBA']; ?><!--">Edit</a>-->
                        </div>
                    </div>
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-2.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!---->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-3.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-2.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-3.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-2.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-2.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="dosen card" style="width:200px">-->
                    <!--                    <img class="card-img-top" src="contents/img/team/team-3.jpg" alt="Card image" style="width:100%">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h4 class="card-title text-center">John Doe</h4>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                <?php } ?>
            </div>
        </div>
    </section>
</main>
</html>
<?php include "layout/footerIndex.php"; ?>

