<?php
session_start();

if ($_GET['halaman'] == "export-data-tim") {
    echo header('Content-type: application/vnd-ms-excel');
    echo header('Content-Disposition: attachment; filename=Data Tim UMKM.xls');
}else {
        echo '';
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../contents/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../contents/img/logo-umkm22.png">
  <title>
    SI UMKM PENS
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="../contents/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../contents/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>

  <link href="../contents/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="../contents/css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />
    <link id="pagestyle" rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
</head>

