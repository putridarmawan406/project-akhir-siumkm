<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";

require_once "../API/timKewirausahaan.php";
$hasilTimKewirausahaan = query_viewTimKewirausahaan();
oci_fetch_all($hasilTimKewirausahaan, $rowTimKewirausahaan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/dosenPembimbing.php";
$hasilDosbim = query_viewDosbim();
oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/status-validasi.php";
$hasilStatus = query_viewStatus();
oci_fetch_all($hasilStatus, $rowStatus, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

//var_dump($_SESSION);
?>
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
          <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                  <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                  </div>
              </a>
          </li>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Logbook</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Tambah Logbook</h6>
        </nav>
          <ul class="navbar-nav  justify-content-end">
              <div class="nav-item dropdown">
                  <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-user me-sm-1"></i>
                      <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php
                      if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                          echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                      }
                      ?>
                      <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                  </ul>
              </div>
          </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Upload Logbook Tim UMKM</h6>
            </div>
                    <div class="container-fluid py-4">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <input name="idTim" class="form-control" type="hidden" value="<?= $_SESSION["id"] ?>" id="timUMKM" placeholder="tim UMKM">
                        </div>
                        <div class="form-group">
                            <label for="judul-logbook" class="form-control-label">Judul Logbook</label>
                            <input name="judulLogbook" class="form-control" type="text" value="" id="judul-logbook" placeholder="Judul Logbook" required>
                        </div>
                        <div class="form-group">
                            <label for="tanggal-upload" class="form-control-label">Tanggal Upload</label>
                            <input name="" class="form-control"  type="date" value="<?php echo date("Y-m-d");?>" id="date-input-Logbook" placeholder="Tanggal Upload" required disabled>
                            <input name="tanggalUpload" class="form-control"  type="hidden" value="<?php echo date("Y-m-d");?>" id="date-input-Logbook" placeholder="Tanggal Upload" required>
                        </div>
                        <div class="form-group">
                            <label for="dosbim-tim" class="form-control-label">Dosen Pembimbing Tim UMKM</label><br>
                            <select name="" class="form-control" id="dosbim" disabled required>
                                <?php  foreach ($rowDosbim as $hasilDosbim) {?>
                                    <?php if($_SESSION["dosbim"] == $hasilDosbim['NOMOR']) { ?>
                                        <option value="<?php echo $hasilDosbim['NOMOR']; ?>" selected><?php echo $hasilDosbim['NAMA']; ?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $hasilDosbim['NOMOR']; ?>"><?php echo $hasilDosbim['NAMA']; ?></option>
                                    <?php } ?>

                                <?php } ?>
                            </select>
                            <input name="dosbim" class="form-control" type="hidden" value="<?php echo $_SESSION["dosbim"]; ?>" id="dosbim" placeholder="">
                        </div>
                        <div class="mb-3">
                        <label for="formFile" class="form-label">Tambahkan File (Pdf)</label>
                        <input name="fileLogbook" class="form-control" type="file" id="formFile" required>
                      </div>
                        <div class="form-group">
                            <label for="status" class="form-control-label">Status Laporan</label>
                            <select name="" class="form-control" id="statusLogbook" disabled required>
<!--                                <option value="" disabled selected>Status</option>-->
                                <?php  foreach ($rowStatus as $hasilStatus) {?>
                                    <option value="<?php echo $hasilStatus['ID_STATUS']; ?>"><?php echo $hasilStatus['JENIS_STATUS']; ?></option>
                                <?php } ?>
                            </select>
                            <input name="statusLogbook" class="form-control" type="hidden" value="1" id="statusLogbook">
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=logbook" role="button">Back</a>
                    </form>
                        <?php
                        if (isset($_POST['save'])) {
                            $name = $_POST['judulLogbook'];
                            $ekstensi_diperbolehkan = array('pdf');
                            $nama = $_FILES['fileLogbook']['name'];
                            $x = explode('.', $nama);
                            $ekstensi = strtolower(end($x));
                            $ukuran = $_FILES['fileLogbook']['size'];
                            $file_tmp = $_FILES['fileLogbook']['tmp_name'];
                            $waktu = $_POST['tanggalUpload'];
                            $dosbim = $_POST['dosbim'];
                            $id = $_POST['idTim'];
                            $status = $_POST['statusLogbook'];

                            if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
                                if ($ukuran < 8044070) {
                                    move_uploaded_file($file_tmp, '/var/www/html/mis134/file_logbook/' . $nama);
//                                    $query = mysql_query("INSERT INTO upload VALUES(NULL, '$nama')");

                                    $sql = "INSERT INTO LOGBOOK (JUDUL_LOGBOOK,TANGGAL_UPLOAD,ID_DOSBIM,FILE_LOGBOOK,ID_TIM,ID_LOGBOOK,STATUS_LOGBOOK)
                                            VALUES ('$name',TO_DATE('$waktu','yyyy/mm/dd'),'$dosbim','$nama','$id',logbook_seq.NEXTVAL,'$status')";
                                    $hasil = query_insert($conn, $sql);
                                    if ($hasil) {
                                        echo "<script>alert('Data Berhasil Disimpan');</script>";
                                        echo "<script>location='home.php?halaman=logbook';</script>";
                                    } else {
                                        echo "<script>alert('Gagal Mengupload File');</script>";
                                        echo "<script>location='home.php?halaman=tambah-logbook';</script>";
                                    }
                                } else {
                                    echo "<script>alert('File terlalu besar');</script>";
                                    echo "<script>location='home.php?halaman=tambah-logbook';</script>";
                                }
                            } else {
                                echo "<script>alert('EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN');</script>";
                                echo "<script>location='home.php?halaman=tambah-logbook';</script>";
                            }
                        }
                        ?>
                </div>
            </div>
        </div>
        </div>
    </div>