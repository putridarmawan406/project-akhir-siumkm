<?php

require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM LOMBA L 
        WHERE L.ID_LOMBA = :v1 ";

$hasilLomba = query_detail($conn, $sql , $data);
oci_fetch_all($hasilLomba, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$fotoLomba=$rows[0]['POSTER_LOMBA'];

if(file_exists("../foto_lomba/$fotoLomba")){
    unlink("../foto_lomba/$fotoLomba");
}

$sql2 = "DELETE FROM LOMBA WHERE ID_LOMBA=:v1";
$hasil = query_delete($conn, $sql2,$data);

echo "<script>alert('Data berhasil dihapus');</script>";
echo "<script>location='home.php?halaman=data-lomba';</script>";
?>

