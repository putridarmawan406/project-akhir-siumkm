<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM LOGBOOK L 
        WHERE L.ID_LOGBOOK = :v1 ";

$hasilLogbook = query_detail($conn, $sql , $data);
oci_fetch_all($hasilLogbook, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$fileLogbook=$rows[0]['FILE_LOGBOOK'];

if(file_exists("../file_logbook/$fileLogbook")){
    unlink("../file_logbook/$fileLogbook");
}

$sql2 = "DELETE FROM LOGBOOK WHERE ID_LOGBOOK=:v1";
$hasil = query_delete($conn, $sql2,$data);

echo "<script>alert('Data berhasil dihapus');</script>";
echo "<script>location='home.php?halaman=logbook';</script>";
?>

