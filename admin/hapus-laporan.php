<?php

require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM LAPORAN P 
        WHERE P.ID_LAPORAN = :v1 ";

$hasilLaporan = query_detail($conn, $sql , $data);
oci_fetch_all($hasilLaporan, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$fileLaporan=$rows[0]['FILE_LAPORAN'];

if(file_exists("../file_laporan/$fileLaporan")){
    unlink("../file_laporan/$fileLaporan");
}

$sql2 = "DELETE FROM LAPORAN WHERE ID_LAPORAN=:v1";
$hasil = query_delete($conn, $sql2,$data);

echo "<script>alert('Data berhasil dihapus');</script>";
echo "<script>location='home.php?halaman=laporan';</script>";
?>

