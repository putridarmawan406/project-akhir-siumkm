<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM LAPORAN L 
        JOIN PEGAWAI P ON P.NOMOR=L.ID_DOSBIM   
        WHERE L.ID_LAPORAN = :v1 ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}
require_once "../API/dosenPembimbing.php";
$hasilDosbim = query_viewDosbim();
oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

include "../API/timKewirausahaan.php";
$hasilKWU = query_viewTimKewirausahaan();
oci_fetch_all($hasilKWU, $rowsKWU, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/status-validasi.php";
$hasilStatus = query_viewStatus();
oci_fetch_all($hasilStatus, $rowStatus, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Validasi Laporan</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Validasi Laporan</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Validasi Laporan Tim UMKM</h6>
                </div>
                <div class="container-fluid py-4">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <input name="idTim" class="form-control" type="hidden" value="<?php echo $hasil['ID_TIM']; ?>" id="timUMKM" placeholder="tim UMKM">
                        </div>
                        <div class="form-group">
                            <label for="judul-laporan" class="form-control-label">Judul Laporan</label>
                            <input name="" class="form-control" type="text" value="<?php echo $hasil['JUDUL_LAPORAN']; ?>" id="nama-tim" placeholder="Judul Laporan" disabled>
                            <input name="judulLaporan" class="form-control" type="hidden" value="<?php echo $hasil['JUDUL_LAPORAN']; ?>" id="nama-tim" placeholder="Judul Laporan">
                        </div>
                        <div class="form-group">
                            <label for="tanggal-upload" class="form-control-label">Tanggal Upload</label>
                            <input name="" class="form-control" type="date" value="<?php echo date('Y-m-d',strtotime($hasil['TANGGAL_UPLOAD'])); ?>" id="date-input-laporan" placeholder="Tanggal Upload" disabled>
                            <input name="tanggalUpload" class="form-control" type="hidden" value="<?php echo date('Y-m-d',strtotime($hasil['TANGGAL_UPLOAD'])); ?>" id="date-input-laporan" placeholder="Tanggal Upload">
                        </div>
                        <label for="dosbim-tim" class="form-control-label">Dosen Pembimbing Tim UMKM</label><br>
                        <div class="form-group">
                            <select name="" class="form-control" id="dosbim" disabled>
                                <?php  foreach ($rowDosbim as $hasilDosbim) {?>
                                    <?php if($hasil['ID_DOSBIM'] == $hasilDosbim['NOMOR']) { ?>
                                        <option value="<?php echo $hasilDosbim['NOMOR']; ?>" selected><?php echo $hasilDosbim['NAMA']; ?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $hasilDosbim['NOMOR']; ?>"><?php echo $hasilDosbim['NAMA']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="dosbim" class="form-control" type="hidden" value="<?php echo $hasil['ID_DOSBIM']; ?>" id="dosbim" placeholder="">
                        </div>
                        <div class="mb-3">
                            <label for="formFile" class="form-label">File Laporan</label>
                            <br>
                            <iframe src="../file_laporan/<?=$hasil['FILE_LAPORAN']?>" width="900" height="500"></iframe>
                            <input name="fileLaporan" class="form-control" type="file" value="<?php echo $hasil['FILE_LAPORAN']; ?>" id="formFile">
                        </div>
                        <div class="form-group">
                            <label for="status" class="form-control-label">Status Laporan</label>
                            <select name="statusLaporan" class="form-control" id="statusLaporan">
                                <?php  foreach ($rowStatus as $hasilStatus) {?>
                                    <?php if($hasil['STATUS_LAPORAN'] == $hasilStatus['ID_STATUS']) { ?>
                                        <option value="<?php echo $hasilStatus['ID_STATUS']; ?>" selected><?php echo $hasilStatus['JENIS_STATUS']; ?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $hasilStatus['ID_STATUS']; ?>"><?php echo $hasilStatus['JENIS_STATUS']; ?></option>
                                    <?php } ?>

                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="catatan" class="form-control-label">Catatan Dosen Pembimbing</label>
                            <input name="catatan" class="form-control" type="text" value="<?php echo $hasil['CATATAN_LAPORAN']; ?>" id="catatan" placeholder="masukkan catatan">
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=laporan" role="button">Back</a>
                    </form>
                    <?php
                    if (isset($_POST['save']))
                    {
                        $name = $_POST['judulLaporan'];
                        $ekstensi_diperbolehkan = array('pdf', '');
                        $nama = $_FILES['fileLaporan']['name'] == '' ? $hasil['FILE_LAPORAN'] : $_FILES['fileLaporan']['name'];
                        $x = explode('.', $nama);
                        $ekstensi = strtolower(end($x));
                        $ukuran = $_FILES['fileLaporan']['size'];
                        $file_tmp = $_FILES['fileLaporan']['tmp_name'];
                        $waktu = $_POST['tanggalUpload'];
                        $dosbim = $_POST['dosbim'];
                        $id = $_POST['idTim'];
                        $catatan = $_POST['catatan'];
                        $status = $_POST['statusLaporan'];

                        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
                            if ($ukuran < 8044070) {
                                move_uploaded_file($file_tmp, '../file_laporan/' . $nama);

                                $sql = "UPDATE LAPORAN SET JUDUL_LAPORAN = '$name',TANGGAL_UPLOAD = TO_DATE('$waktu','yyyy/mm/dd'),ID_DOSBIM = '$dosbim',FILE_LAPORAN = '$nama',ID_TIM = '$id',CATATAN_LAPORAN = '$catatan',STATUS_LAPORAN = '$status' WHERE ID_LAPORAN=:v1";
                                $hasil = query_update($conn, $sql, $data);
                                if ($hasil) {
                                    echo "<script>alert('Data Berhasil Disimpan');</script>";
                                    echo "<script>location='home.php?halaman=laporan';</script>";
                                } else {
                                    echo "<script>alert('Gagal Mengupload File');</script>";
                                }
                            } else {
                                echo "<script>alert('File terlalu besar');</script>";
                            }
                        } else {
                            echo "<script>alert('EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN');</script>";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
