    <?php 
    require_once "../API/koneksi.php";
	require "../includes/func.inc.php";
    require_once "../API/kategori.php";
    $hasil = query_viewKategori();
        oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    require_once "../API/dosenPembimbing.php";
    $hasilDosbim = query_viewDosbim();
        oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    require_once "../API/jurusan.php";
    $hasilJurusan = query_viewJurusan();
    oci_fetch_all($hasilJurusan, $rowJurusan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    include "../API/timKewirausahaan.php";
    $hasilTim = query_viewTimKewirausahaan();
    oci_fetch_all($hasilTim, $rowsTim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    ?>
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
          <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                  <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                  </div>
              </a>
          </li>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Data Tim</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Tambah Data Tim</h6>
        </nav>
          <ul class="navbar-nav  justify-content-end">
              <div class="nav-item dropdown">
                  <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-user me-sm-1"></i>
                      <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php
                      if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                          echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                      }
                      ?>
                      <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                  </ul>
              </div>
          </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Tambah Data Tim UMKM</h6>
            </div>
                    <div class="container-fluid py-4">
                    <form method="POST">
                        <div class="form-group">
                            <label for="nama-tim" class="form-control-label">Nama Tim UMKM</label>
                            <input name="namaTimUMKM" class="form-control" type="text" value="" id="nama-tim" placeholder="Nama Tim UMKM" required>
                        </div>
                        <div class="form-group">
                            <label for="email-tim" class="form-control-label">Email Tim UMKM</label>
                            <input name="emailTimUMKM" class="form-control" type="text" value="" id="email-tim" placeholder="Email Tim UMKM" required>
                        </div>
                        <div class="form-group">
                            <label for="password-email" class="form-control-label">Password Email</label>
                            <input name="password" class="form-control" type="text" value="" id="password-email" placeholder="Password Email" required>
                        </div>
                        <div class="form-group">
                            <label for="kategori" class="form-control-label">Kategori Tim UMKM</label>
                            <!-- <input name="kategoriUMKM" class="form-control" type="text" value="" id="kategori" placeholder="Kategori Tim UMKM"> -->
                            <select name="kategoriUMKM" class="form-control" id="kategoriUMKM" required>
                              <option value="" disabled selected>kategori UMKM</option>
                              <?php  foreach ($rows as $hasil) {?>
                              <option value="<?php echo $hasil['ID_KATEGORI']; ?>"><?php echo $hasil['NAMA_KATEGORI']; ?></option>
                              <?php } ?>
                              </select>
                          </div>
                        <div class="form-group">
                            <label for="dosbim-tim" class="form-control-label">Dosen Pembimbing Tim UMKM</label><br>
                            <select name="dosenPembimbing" class="form-control" id="dosbim" required>
                              <option value="" disabled selected>Dosen Pembimbing</option>
                              <?php  foreach ($rowDosbim as $hasilDosbim) {?>
                                 <option value="<?php echo $hasilDosbim['NOMOR']; ?>"><?php echo $hasilDosbim['NAMA']; ?></option>
                              <?php } ?>
                            </select>
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=data-tim" role="button">Back</a>
                        </form>
                        <?php 
                        if (isset($_POST['save']))
                        {
                          $nama = $_POST['namaTimUMKM'];
                          $email = $_POST['emailTimUMKM'];
                          $password = $_POST['password'];
                          $kategori = $_POST['kategoriUMKM'];
                          $dosbim = $_POST['dosenPembimbing'];
//                          $ketua = $_POST['namaKetua'];
//                          $jurusan = $_POST['jurusanKetua'];
                          $sql = "INSERT INTO TIM_KEWIRAUSAHAAN (NAMA_TIM,EMAIL,PASSWORD,KATEGORI,ID_TIM,DOSBIM) 
                                  VALUES ('$nama','$email','$password','$kategori',tim_seq.NEXTVAL,'$dosbim') RETURNING ID_TIM INTO :p_val";
                            $statement = oci_parse($conn, $sql);
                            oci_bind_by_name($statement, ":p_val", $idNumber,10);
                            oci_execute($statement);

	                        echo "<script>location='home.php?halaman=tambah-data-anggota&id=$idNumber';</script>";
                        }
                        ?>
                </div>
            </div>
        </div>
        </div>
    </div>