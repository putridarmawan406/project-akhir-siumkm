<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
require_once "../API/kategori.php";

$hasilKategori = query_viewKategori();
oci_fetch_all($hasilKategori, $rowsKategori, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = "
        SELECT TM.* , KU.* , P.* , P.NAMA as NAMA_DOSEN 
        FROM TIM_KEWIRAUSAHAAN TM 
		JOIN KATEGORI_UMKM KU ON TM.KATEGORI=KU.ID_KATEGORI 
		JOIN PEGAWAI P ON P.NOMOR=TM.DOSBIM   
		WHERE P.STAFF=4 AND TM.ID_TIM = :v1
    ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}

$sql2 = "
        SELECT * 
        FROM ANGGOTA_TIM A  
        JOIN MAHASISWA M ON M.NRP=A.ANGGOTA AND A.ID_TIM = :v1
    ";

$hasil2 = query_detail($conn, $sql2 , $data);
oci_fetch_all($hasil2, $rows2, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows2 as $hasil2) {
    $item2[] = $hasil2;
}

require_once "../API/dosenPembimbing.php";
$hasilDosbim = query_viewDosbim();
oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/dataMahasiswa.php";
$hasilMahasiswa = query_viewDataMahasiswa();
oci_fetch_all($hasilMahasiswa, $rowMahasiswa, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/dataAnggota.php";
$hasilAnggota = query_viewDataAnggota();
oci_fetch_all($hasilAnggota, $rowAnggota, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Detail Data Tim</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Data Tim</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Detail Data Tim UMKM</h6>
                </div>
                <div class="container-fluid py-4">
                    <form>
                        <div class="row">
                        <div class="col-6">
                        <div class="form-group">
                            <h6>Nama Tim UMKM</h6>
                            <h8><?php echo $hasil['NAMA_TIM']; ?></h8>
                        </div>
                        <div class="form-group">
                            <h6>Email Tim UMKM</h6>
                            <h8><?php echo $hasil['EMAIL']; ?></h8>
                        </div>
                        <div class="form-group">
                            <h6>Kategori Pendanaan</h6>
                            <h8 name="kategoriUMKM" type="text" value="<?php echo $hasilKategori['KATEGORI']; ?>" id="kategoriUMKM"><?php echo $hasil['NAMA_KATEGORI']; ?></h8>
                        </div>
                        <div class="form-group">
                            <h6>Dosen Pembimbing Tim UMKM</h6>
                            <h8 name="dosbim" type="text" value="<?php echo $hasilDosbim['DOSBIM']; ?>" id="dosbim"><?php echo $hasil['NAMA_DOSEN']; ?></h8>
                        </div>
                        </div>
                        <div class="col-6">
                        <?php  foreach ($rows2 as $hasil2) {?>
                        <div class="form-group">
                            <h6>Anggota</h6>
                            <h8 name="anggota" type="text" value="<?php echo $hasilAnggota['ANGGOTA']; ?>" id="anggota"><?php echo $hasil2['NAMA']; ?></h8>
                        </div>
                        <?php } ?>
                        </div>
                        </div>
                        <a class="btn btn-info" href="home.php?halaman=data-tim" role="button">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>