<?php
require_once "../API/export.php";

$hasilExport1 = query_viewExport1();
oci_fetch_all($hasilExport1, $rowsExport1, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$hasilExport2 = query_viewExport2();
oci_fetch_all($hasilExport2, $rowsExport2, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Tim UMKM.xls");
?>

<h3>Data Tim UMKM</h3>
<div class="card-body px-0 pt-0 pb-2">
    <div class="table-responsive px-3">
        <table class="table align-items-center mb-0" id="myTable">
            <thead>
            <tr>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Nama UMKM</th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Email UMKM</th>
                <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Bidang Pendanaan</th>
                <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Nama Dosen Pembimbing</th>
            </tr>
            </thead>
            <tbody>
            <?php  foreach ($rowsExport1 as $hasil) {?>
                <tr>
                    <td>
                        <div class="d-flex px-3 py-1">
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm"><?php echo $hasil['NAMA_TIM']; ?></h6>
                            </div>
                        </div>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['EMAIL']; ?></p>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['NAMA_KATEGORI']; ?></p>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['NAMA_DOSEN']; ?></p>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<br>
<h3>Data Anggota Tim UMKM</h3>
<div class="card-body px-0 pt-0 pb-2">
    <div class="table-responsive px-3">
        <table class="table align-items-center mb-0" id="myTable">
            <thead>
            <tr>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">NRP</th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Nama</th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Nama Tim</th>
                <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Bidang Pendanaan</th>
                <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Jurusan</th>
            </tr>
            </thead>
            <tbody>
            <?php  foreach ($rowsExport2 as $hasil) {?>
                <tr>
                    <td>
                        <div class="d-flex px-3 py-1">
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm"><?php echo $hasil['ANGGOTA']; ?></h6>
                            </div>
                        </div>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['NAMA']; ?></p>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['NAMA_TIM']; ?></p>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['NAMA_KATEGORI']; ?></p>
                    </td>
                    <td>
                        <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['JURUSAN_LENGKAP']; ?></p>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>