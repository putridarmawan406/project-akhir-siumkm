<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Data lomba</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data lomba</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Tambah Data lomba</h6>
                </div>
                <div class="container-fluid py-4">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nama-lomba" class="form-control-label">Nama lomba</label>
                            <input name="namaLomba" class="form-control" type="text" value="" id="nama-lomba" placeholder="Nama-lomba" required>
                        </div>
                        <div class="form-group">
                            <label for="waktu-lomba" class="form-control-label">Waktu lomba</label>
                            <input name="waktuLomba" class="form-control" type="date" value="" id="date-input-waktu" placeholder="Waktu lomba" required>
                        </div>
                        <div class="form-group">
                            <label for="link-lomba" class="form-control-label">Link lomba</label>
                            <input name="linkLomba" class="form-control" type="text" value="" id="link-lomba" placeholder="Link lomba" required>
                        </div>
                        <div class="mb-3">
                            <label for="formFile" class="form-label">Tambahkan Poster</label>
                            <input name="posterLomba" class="form-control" type="file" id="formFile" required>
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=data-lomba" role="button">Back</a>
                    </form>
                    <?php
                    if (isset($_POST['save']))
                    {
                        $nama = $_POST['namaLomba'];
                        $foto = $_FILES['posterLomba']['name'];
                        $lokasi = $_FILES['posterLomba']['tmp_name'];
                        move_uploaded_file($lokasi, "/var/www/html/mis134/foto_lomba/".$foto);
                        $waktu = $_POST['waktuLomba'];
                        $link = $_POST['linkLomba'];

                        $sql = "INSERT INTO LOMBA (ID_LOMBA,NAMA_LOMBA,WAKTU_LOMBA,POSTER_LOMBA,LINK_LOMBA)
                                VALUES (lomba_seq.NEXTVAL,'$nama',TO_DATE('$waktu','yyyy/mm/dd'),'$foto','$link')";
                        $hasil = query_insert($conn, $sql);

                        echo "<script>alert('Data berhasil ditambahkan');</script>";
                        echo "<script>location='home.php?halaman=data-lomba';</script>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>