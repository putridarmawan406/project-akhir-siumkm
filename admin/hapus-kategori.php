<?php

require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = "DELETE FROM KATEGORI_UMKM WHERE ID_KATEGORI=:v1";
$hasil = query_delete($conn, $sql,$data);

echo "<script>alert('Data Berhasil Dihapus');</script>";
echo "<script>location='home.php?halaman=kategori';</script>";
?>