<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM PELATIHAN P WHERE P.ID_PEL = :v1 ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}
?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Data Pelatihan</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Pelatihan</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Tambah Data Pelatihan</h6>
                </div>
                <div class="container-fluid py-4">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nama-pelatihan" class="form-control-label">Nama Pelatihan</label>
                            <input name="namaPelatihan" class="form-control" type="text" value="<?php echo $hasil['NAMA_PEL']; ?>" id="nama-pelatihan" placeholder="Nama-pelatihan" required>
                        </div>
                        <div class="form-group">
                            <label for="waktu-pelatihan" class="form-control-label">Waktu Pelatihan</label>
                            <input name="waktuPelatihan" class="form-control" type="date" value="<?php echo date('Y-m-d',strtotime($hasil['WAKTU_PEL'])); ?>" id="date-input-waktu" placeholder="Waktu Pelatihan" required>
                        </div>
                        <div class="mb-3">
                            <label for="formFile" class="form-label">Tambahkan Poster</label>
                            <?php
                                if($hasil['POSTER_PEL']){
                            ?>
                                    <div class="mb-2 d-flex flex-column">
                                        <span class="mb-2" name="old" value="<?=$hasil['POSTER_PEL']?>"><?php echo $hasil['POSTER_PEL'];?></span>
                                        <img src="../foto_pelatihan/<?=$hasil['POSTER_PEL']?>" alt="" style="max-width: 500px;">
                                    </div>
                            <?php } ?>
                            <input name="posterPelatihan" class="form-control" type="file" value="<?php echo $hasil['POSTER_PEL']; ?>" id="formFile" >
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=data-pelatihan" role="button">Back</a>
                    </form>
                    <?php
                    if (isset($_POST['save']))
                    {
                        $nama = $_POST['namaPelatihan'];
                        $foto = $_FILES['posterPelatihan']['name'] == '' ? $hasil['POSTER_PEL'] : $_FILES['posterPelatihan']['name'];
                        $lokasi = $_FILES['posterPelatihan']['tmp_name'];
                        move_uploaded_file($lokasi, "../foto_pelatihan/".$foto);
                        $waktu = $_POST['waktuPelatihan'];

                        $sql = "UPDATE PELATIHAN SET NAMA_PEL='$nama',WAKTU_PEL=TO_DATE('$waktu','yyyy/mm/dd'),POSTER_PEL='$foto' WHERE ID_PEL=:v1";
                        $hasil = query_update($conn, $sql, $data);

                        echo "<script>alert('Data berhasil diedit');</script>";
                        echo "<script>location='home.php?halaman=data-pelatihan';</script>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>