<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Profil Pengguna</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Profil Pengguna</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Detail Profil Pengguna</h6>
                </div>
                <div class="container-fluid py-4">
                    <form>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <h6>Nama Pengguna</h6>
                                    <h8><?php echo $_SESSION['Nama']; ?></h8>
                                </div>
                                <div class="form-group">
                                    <h6>Email Pengguna</h6>
                                    <?php
                                    if($_SESSION['StatusPengguna'] == "Pegawai") {
                                        echo "<h8>{$_SESSION['Netid']}</h8>";
                                    } elseif ($_SESSION['StatusPengguna'] == "Mahasiswa"){
                                        echo "<h8>{$_SESSION['email']}</h8>";
                                    }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <h6>Status Pengguna</h6>
                                    <h8 name="status" type="text" value="" id="status"><?php echo $_SESSION['StatusPengguna']; ?></h8>
                                </div>
                            </div>
                        </div>
                        <?php
                        if($_SESSION['StatusPengguna'] == "Pegawai") {
                            echo "<a class='btn btn-info' href='home.php?halaman=dashboard'>Back</a>";
                        }
                        elseif($_SESSION['StatusPengguna'] == "Mahasiswa") {
                            echo "<a class='btn btn-info' href='home.php?halaman=edit-profil-mhs'>Ubah Password</a> &nbsp";
                            echo "<a class='btn btn-secondary' href='home.php?halaman=dashboard'>Back</a>";
                        }
                        ?>
                    </form>
            </div>
        </div>
    </div>
</div>