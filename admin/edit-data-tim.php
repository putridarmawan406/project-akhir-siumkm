    <?php 
    require_once "../API/koneksi.php";
    require "../includes/func.inc.php";
    require_once "../API/kategori.php";

    $hasilKategori = query_viewKategori();
    oci_fetch_all($hasilKategori, $rowsKategori, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    $nomor = $_GET['id'];

    $data = array(
      ':v1' => $nomor
      );

    $sql = "
        SELECT * FROM TIM_KEWIRAUSAHAAN TM 
		JOIN KATEGORI_UMKM KU ON TM.KATEGORI=KU.ID_KATEGORI 
		JOIN PEGAWAI P ON P.NOMOR=TM.DOSBIM WHERE P.STAFF=4 AND TM.ID_TIM = :v1
    ";
  
    $hasil = query_detail($conn, $sql , $data);
    oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    foreach ($rows as $hasil) {
      $item[] = $hasil;
    }

    $sql2 = "
        SELECT * FROM ANGGOTA_TIM A 
        JOIN MAHASISWA M ON A.ANGGOTA=M.NRP
        JOIN KELAS K ON M.KELAS=K.NOMOR 
		JOIN JURUSAN J ON J.NOMOR=K.JURUSAN
        WHERE A.ID_TIM = :v1
    ";

    $hasil2 = query_detail($conn, $sql2 , $data);

    oci_fetch_all($hasil2, $rows2, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);


    if (count($rows2) >= 1){
        foreach ($rows2 as $hasil2) {
            $item2[] = $hasil2;
        }
        $jumlahData = count($item2);
    }else{
        $jumlahData = 0;
    }
    require_once "../API/jurusan.php";
    $hasilJurusan = query_viewJurusan();
    oci_fetch_all($hasilJurusan, $rowJurusan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    require_once "../API/dataAnggota.php";
    $hasilAnggota = query_viewDataAnggota();
    oci_fetch_all($hasilAnggota, $rowAnggota, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

    require_once "../API/dosenPembimbing.php";
    $hasilDosbim = query_viewDosbim();
    oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
    
    ?>
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
          <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                  <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                  </div>
              </a>
          </li>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Edit Data Tim</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Edit Data Tim</h6>
        </nav>
          <ul class="navbar-nav  justify-content-end">
              <div class="nav-item dropdown">
                  <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-user me-sm-1"></i>
                      <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php
                      if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                          echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                      }
                      ?>
                      <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                  </ul>
              </div>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Edit Data Tim UMKM</h6>
            </div>
                    <div class="container-fluid py-4">
                    <form method="POST">
                        <div class="form-group">
                            <label for="nama-tim" class="form-control-label">Nama Tim UMKM</label>
                            <input name="namaTimUMKM" class="form-control" type="text" value="<?php echo $hasil['NAMA_TIM']; ?>" id="nama-tim" placeholder="Nama Tim UMKM" required>
                        </div>
                        <div class="row">
                        <div class="col-6">
                        <div class="form-group">
                            <label for="email-tim" class="form-control-label">Email Tim UMKM</label>
                            <input name="emailTimUMKM" class="form-control" type="text" value="<?php echo $hasil['EMAIL']; ?>" id="email-tim" placeholder="Email Tim UMKM" required>
                        </div>
                        <div class="form-group">
                            <label for="password-email" class="form-control-label">Password Email</label>
                            <input name="password" class="form-control" type="text" value="<?php echo $hasil['PASSWORD']; ?>" id="password-email" placeholder="Password Email" required>
                        </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="kategori" class="form-control-label">Kategori Pendanaan</label>
                                <select name="kategoriUMKM" class="form-control" id="kategoriUMKM" required>
                                    <?php  foreach ($rowsKategori as $hasilKategori) {?>
                                        <?php if($hasil['ID_KATEGORI'] == $hasilKategori['ID_KATEGORI']) { ?>
                                            <option value="<?php echo $hasilKategori['ID_KATEGORI']; ?>" selected><?php echo $hasilKategori['NAMA_KATEGORI']; ?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $hasilKategori['ID_KATEGORI']; ?>"><?php echo $hasilKategori['NAMA_KATEGORI']; ?></option>
                                        <?php } ?>

                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="dosbim-tim" class="form-control-label">Dosen Pembimbing Tim UMKM</label><br>
                                <select name="dosenPembimbing" class="form-control" id="dosbim" required>
                                    <?php  foreach ($rowDosbim as $hasilDosbim) {?>
                                        <?php if($hasil['DOSBIM'] == $hasilDosbim['NOMOR']) { ?>
                                            <option value="<?php echo $hasilDosbim['NOMOR']; ?>" selected><?php echo $hasilDosbim['NAMA']; ?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $hasilDosbim['NOMOR']; ?>"><?php echo $hasilDosbim['NAMA']; ?></option>
                                        <?php } ?>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=data-tim" role="button">Back</a>
                        </form>
                        <?php 
                        if (isset($_POST['save']))
                        {
                          $nama = $_POST['namaTimUMKM'];
                          $email = $_POST['emailTimUMKM'];
                          $password = $_POST['password'];
                          $kategori = $_POST['kategoriUMKM'];
                          $dosbim = $_POST['dosenPembimbing'];
                          $sql = "UPDATE TIM_KEWIRAUSAHAAN SET NAMA_TIM='$nama',EMAIL='$email',PASSWORD='$password',KATEGORI='$kategori',DOSBIM = '$dosbim' WHERE ID_TIM=:v1";
                          $hasil = query_update($conn, $sql, $data);

                           echo "<script>alert('Data Berhasil Diedit');</script>";
	                        echo "<script>location='home.php?halaman=data-tim';</script>";
                        }
                        ?>
                </div>
            </div>
        </div>
        </div>
        <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>Data Tim UMKM</h6>
            </div>
            <div class="ms-auto">
                <?php if ($jumlahData < 5) {?>
                <a class="btn btn-link text-info text-gradient px-3 mb-0" href="home.php?halaman=tambah-data-anggota-satu&id=<?= $rows[0]['ID_TIM'] ?>"><i class="far fa-plus-square me-2"></i>Tambah</a>
                <?php }?>

            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">NRP</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Jurusan</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach ($rows2 as $hasil2) {?>
                        <tr>
                            <tr>
                                <td>
                                    <div class="d-flex px-3 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm"><?php echo $hasil2['ANGGOTA'] ?></h6>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0 text-center" value="<?php echo $hasil2['ANGGOTA']; ?>"><?php echo $hasil2['NAMA']; ?></p>
                                </td>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0 text-center" value="<?php echo $hasil2['ANGGOTA']; ?>"><?php echo $hasil2['JURUSAN']; ?></p>
                                </td>
                                <td class="align-middle text-center">
                                    <div class="ms-auto">
                                        <a class="btn btn-link text-dark px-3 mb-0" href="home.php?halaman=hapus-data-anggota&id=<?php echo $hasil2['ID_ANGGOTA_TIM'];?>"><i class="fas fa-trash-alt text-red me-2" aria-hidden="true"></i>Hapus</a>
                                    </div>
                                </td>
                            </tr>
                        </tr>
                        <?php } ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>