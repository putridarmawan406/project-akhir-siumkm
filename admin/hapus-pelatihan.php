<?php

require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM PELATIHAN P 
        WHERE P.ID_PEL = :v1 ";

$hasilPel = query_detail($conn, $sql , $data);
oci_fetch_all($hasilPel, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$fotoPelatihan=$rows[0]['POSTER_PEL'];

if(file_exists("../foto_pelatihan/$fotoPelatihan")){
    unlink("../foto_pelatihan/$fotoPelatihan");
}

$sql2 = "DELETE FROM PELATIHAN WHERE ID_PEL=:v1";
$hasil = query_delete($conn, $sql2,$data);

echo "<script>alert('Data berhasil dihapus');</script>";
echo "<script>location='home.php?halaman=data-pelatihan';</script>";
?>

