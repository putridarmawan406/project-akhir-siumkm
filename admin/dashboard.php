 <?php
 require_once "../API/koneksi.php";
        require "../includes/func.inc.php";
        require_once "../API/timKewirausahaan.php";
        $hasil = query_viewTimKewirausahaan();
        oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

         require_once "../API/jumlahTim.php";
         $hasilJumlahTim = query_countJumlahTim();
         oci_fetch_all($hasilJumlahTim, $rowsJumlahTim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_tim = intval($rowsJumlahTim[0]['JUMLAH']);

         require_once "../API/lomba.php";
         $hasilLomba = query_viewLomba();
         oci_fetch_all($hasilLomba, $rowsLomba, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

         require_once "../API/jumlahLomba.php";
         $hasilJumlahLomba = query_countJumlahLomba();
         oci_fetch_all($hasilJumlahLomba, $rowsJumlahLomba, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_lomba = intval($rowsJumlahLomba[0]['JUMLAHLOMBA']);

         require_once "../API/pelatihan.php";
         $hasilPelatihan = query_viewPelatihan();
         oci_fetch_all($hasilPelatihan, $rowsPelatihan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

         require_once "../API/jumlahPelatihan.php";
         $hasilJumlahPelatihan = query_countJumlahPelatihan();
         oci_fetch_all($hasilJumlahPelatihan, $rowsJumlahPelatihan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_pelatihan = intval($rowsJumlahPelatihan[0]['JUMLAHPEL']);

         require_once "../API/jumlahJurusan.php";

         $hasilJumlahTotal = query_JumlahTotal();
         oci_fetch_all($hasilJumlahTotal, $rowsJumlahTotal, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Total = count($rowsJumlahTotal);

         $hasilJumlahIT = query_JumlahAnggotaIT();
         oci_fetch_all($hasilJumlahIT, $rowsJumlahIT, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_IT = count($rowsJumlahIT);

         $hasilJumlahElka = query_JumlahAnggotaElka();
         oci_fetch_all($hasilJumlahElka, $rowsJumlahElka, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Elka = count($rowsJumlahElka);

         $hasilJumlahTelkom = query_JumlahAnggotaTelkom();
         oci_fetch_all($hasilJumlahTelkom, $rowsJumlahTelkom, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Telkom = count($rowsJumlahTelkom);

         $hasilJumlahElin = query_JumlahAnggotaElin();
         oci_fetch_all($hasilJumlahElin, $rowsJumlahElin, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Elin = count($rowsJumlahElin);

         $hasilJumlahMeka = query_JumlahAnggotaMeka();
         oci_fetch_all($hasilJumlahMeka, $rowsJumlahMeka, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Meka = count($rowsJumlahMeka);

         $hasilJumlahTekkom = query_JumlahAnggotaTekkom();
         oci_fetch_all($hasilJumlahTekkom, $rowsJumlahTekkom, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Tekkom = count($rowsJumlahTekkom);

         $hasilJumlahMMB = query_JumlahAnggotaMMB();
         oci_fetch_all($hasilJumlahMMB, $rowsJumlahMMB, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_MMB = count($rowsJumlahMMB);

         $hasilJumlahSPE = query_JumlahAnggotaSPE();
         oci_fetch_all($hasilJumlahSPE, $rowsJumlahSPE, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_SPE = count($rowsJumlahSPE);

         $hasilJumlahGame = query_JumlahAnggotaGame();
         oci_fetch_all($hasilJumlahGame, $rowsJumlahGame, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_Game = count($rowsJumlahGame);

         $hasilJumlahTRI = query_JumlahAnggotaTRI();
         oci_fetch_all($hasilJumlahTRI, $rowsJumlahTRI, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_TRI = count($rowsJumlahTRI);

         $hasilJumlahTRM = query_JumlahAnggotaTRM();
         oci_fetch_all($hasilJumlahTRM, $rowsJumlahTRM, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_TRM = count($rowsJumlahTRM);

         $hasilJumlahDataSains = query_JumlahAnggotaDataSains();
         oci_fetch_all($hasilJumlahDataSains, $rowsJumlahDataSains, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
         $jumlah_DataSains = count($rowsJumlahDataSains);

// var_dump($_SESSION);
        ?>

 <!DOCTYPE html>
 <html>
 <head>
     <link rel="stylesheet" type="text/css" href="style.css"/>
 </head>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
          <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                  <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                  </div>
              </a>
          </li>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Dashboard</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Dashboard</h6>
        </nav>
          <ul class="navbar-nav  justify-content-end">
              <div class="nav-item dropdown">
                  <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-user me-sm-1"></i>
                      <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php
                      if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                          echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                      }
                      ?>
                      <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                  </ul>
              </div>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
 <div class="container-fluid py-4">
     <div class="row">
         <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
             <div class="card">
                 <div class="card-body p-3">
                     <div class="row">
                         <div class="col-8">
                             <div class="numbers">
                                 <p class="text-sm mb-0 text-capitalize font-weight-bold">Tim UMKM</p>
                                 <h5 class="font-weight-bolder mb-0">
                                     <?php echo $jumlah_tim?>
                                     <span class="text-success text-sm font-weight-bolder"></span>
                                 </h5>
                             </div>
                         </div>
                         <div class="col-4 text-end">
                             <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                 <i class="fas fa-home text-lg opacity-10" aria-hidden="true"></i>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
             <div class="card">
                 <div class="card-body p-3">
                     <div class="row">
                         <div class="col-8">
                             <div class="numbers">
                                 <p class="text-sm mb-0 text-capitalize font-weight-bold">Pelatihan</p>
                                 <h5 class="font-weight-bolder mb-0">
                                     <?php echo $jumlah_pelatihan?>
                                     <span class="text-success text-sm font-weight-bolder"></span>
                                 </h5>
                             </div>
                         </div>
                         <div class="col-4 text-end">
                             <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                 <i class="fas fa-chalkboard-teacher text-lg opacity-10" aria-hidden="true"></i>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
             <div class="card">
                 <div class="card-body p-3">
                     <div class="row">
                         <div class="col-8">
                             <div class="numbers">
                                 <p class="text-sm mb-0 text-capitalize font-weight-bold">Lomba</p>
                                 <h5 class="font-weight-bolder mb-0">
                                     <?php echo $jumlah_lomba?>
                                     <span class="text-danger text-sm font-weight-bolder"></span>
                                 </h5>
                             </div>
                         </div>
                         <div class="col-4 text-end">
                             <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                 <i class="fas fa-award text-lg opacity-10" aria-hidden="true"></i>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="row my-4">
         <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
             <div class="card">
                 <div class="card-header pb-0">
                     <div class="row">
                         <div class="col-lg-6 col-7">
                             <h6>Rekap Jumlah Mahasiswa Per Jurusan</h6>
                         </div>
                     </div>
                     <div class="card-body">
                         <p class="d-flex flex-column">
                             <span class="text-bold text-lg">Total <?php echo $jumlah_Total?></span>
                         </p>
                         <div class="row">
                             <div class="col-6">
                                 <div class="progress-group">
                                     Teknik Elektronika
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Elka ?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-warning" style="width: <?php echo ($jumlah_Elka/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>
                                 <!-- /.progress-group -->

                                 <div class="progress-group">
                                     Teknik Telekomunikasi
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Telkom ?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-success" style=" width: <?php echo ($jumlah_Telkom/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknik Elektro Industri
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Elin ?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_Elin/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknik Informatika
                                     <span class="" style="font-weight:bold; float: right"><?php echo $jumlah_IT?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-info" style="width: <?php echo ($jumlah_IT/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknik Mekatronika
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Meka?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar" style="width: <?php echo ($jumlah_Meka/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknik Komputer
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Tekkom?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_Tekkom/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>
                             </div>
                             <!-- /.card -->
                             <div class="col-6">
                                 <div class="progress-group">
                                     Multimedia Broadcasting
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_MMB?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-warning" style="width: <?php echo ($jumlah_MMB/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>
                                 <!-- /.progress-group -->

                                 <div class="progress-group">
                                     Sistem Pembangkit Energi
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_SPE?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-success" style=" width: <?php echo ($jumlah_SPE/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknologi Game
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_Game?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_Game/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknologi Rekayasa Internet
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_TRI?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-info" style="width: <?php echo ($jumlah_TRI/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Teknologi Rekayasa Multimedia
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_TRM?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar" style="width: <?php echo ($jumlah_TRM/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>

                                 <div class="progress-group">
                                     Sains Data Terapan
                                     <span class="float-right" style="font-weight:bold; float: right"><?php echo $jumlah_DataSains?></span>
                                     <div class="progress progress-sm">
                                         <div class="progress-bar bg-danger" style="width: <?php echo ($jumlah_DataSains/$jumlah_Total)*100; ?>%"></div>
                                     </div>
                                 </div> <br>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
      <div class="row my-4">
        <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
          <div class="card">
            <div class="card-header pb-0">
              <div class="row">
                <div class="col-lg-6 col-7">
                  <h6>Pengumuman Kegiatan</h6>
                  <p class="text-sm mb-0">
                    <i class="fas fa-bullhorn text-info" aria-hidden="true"></i>
                    <span class="font ms-1">Timeline kegiatan Kewirausahaan</span>
                  </p>
                </div>
                <div class="col-lg-6 col-5 my-auto text-end">
                  <div class="dropdown float-lg-end pe-4">
                    <a class="cursor-pointer" id="dropdownTable" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-ellipsis-v text-secondary"></i>
                    </a>
                    <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable">
                      <!-- <li><a class="dropdown-item border-radius-md" href="javascript:;">Action</a></li> -->
                      <li><a class="dropdown-item border-radius-md" href="javascript:;">Another action</a></li>
                      <li><a class="dropdown-item border-radius-md" href="javascript:;">Something else here</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-lg text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Informasi</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Poster</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Waktu</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  foreach ($rowsLomba as $hasilLomba) {?>
                    <tr>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="m-0 text-sm"><?php echo $hasilLomba['NAMA_LOMBA']; ?></h6>
                          </div>
                        </div>
                      </td>
                      <td class="align-middle text-sm w-75 mx-auto text-center">
                          <img src="../foto_lomba/<?php echo $hasilLomba['POSTER_LOMBA']; ?>" alt="" style="max-width: 250px;">
                      </td>
                      <td class="align-middle text-center text-sm">
                        <span class="text-xs font-weight-bold"><?php echo $hasilLomba['WAKTU_LOMBA']; ?></span>
                      </td>
                      <td class="align-middle text-center">
                        <div class="ms-auto">
                          <a class="btn btn-link text-info text-gradient px-3 mb-0" href="home.php?halaman=detail-data-lomba-dashboard&id=<?php echo $hasilLomba['ID_LOMBA'];?>"><i class="far fa-eye me-2"></i>Detail</a>
                          </div>
                      </td>
                    </tr> 
                    <?php } ?>
                    <?php  foreach ($rowsPelatihan as $hasilPelatihan) {?>
                        <tr>
                            <td>
                                <div class="d-flex px-3 py-1">
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="m-0 text-sm"><?php echo $hasilPelatihan['NAMA_PEL']; ?></h6>
                                    </div>
                                </div>
                            </td>
                            <td class="align-middle text-sm w-75 mx-auto text-center">
                                <img src="../foto_pelatihan/<?php echo $hasilPelatihan['POSTER_PEL']; ?>" alt="" style="max-width: 250px;">
                            </td>
                            <td class="align-middle text-center text-sm">
                                <span class="text-xs font-weight-bold"><?php echo $hasilPelatihan['WAKTU_PEL']; ?></span>
                            </td>
                            <td class="align-middle text-center">
                                <div class="ms-auto">
                                    <a class="btn btn-link text-info text-gradient px-3 mb-0" href="home.php?halaman=detail-pelatihan-dashboard&id=<?php echo $hasilPelatihan['ID_PEL'];?>"><i class="far fa-eye me-2"></i>Detail</a>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
 </html>
 <footer class="footer pt-3  ">
     <div class="container-fluid">
         <div class="row align-items-center justify-content-lg-between">
             <div class="col-lg-8 mb-lg-0 mb-4">
                 <div class="copyright text-center text-sm text-muted text-lg-start">
                     © <script>
                         document.write(new Date().getFullYear())
                     </script>
                     Sistem Informasi UMKM Politeknik Elektronika Negeri Surabaya
                 </div>
             </div>
         </div>
     </div>
 </footer>
