<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";

require_once "../API/dosenPembimbing.php";
$hasilDosbim = query_viewDosbim();
oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/jurusan.php";
$hasilJurusan = query_viewJurusan();
oci_fetch_all($hasilJurusan, $rowJurusan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Data Anggota Tim</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Anggota Tim</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Tambah Data Anggota Tim UMKM</h6>
                </div>
                <div class="container-fluid py-4">
                    <form method="POST">
                                <div class="form-group">
                                    <label for="timUMKM" class="form-control-label">Nama Tim UMKM</label>
                                    <?php
                                    $id = $_GET['id'];
                                    //                                    print_r($id);
                                    $sql = "SELECT * FROM TIM_KEWIRAUSAHAAN WHERE ID_TIM = :v1";
                                    $data = array(':v1' => $id);
                                    $hasil = query_view($conn, $sql, $data);

                                    oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
                                    ?>
                                    <input name="timUMKM" class="form-control" type="hidden" value="<?= $rows[0]['ID_TIM'] ?>" id="timUMKM" placeholder="tim UMKM">
                                    <input name="" class="form-control" type="text" value="<?= $rows[0]['NAMA_TIM'] ?>" id="timUMKM" placeholder="tim UMKM" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="anggota" class="form-control-label">Nama Anggota</label>
                                    <input name="nrpAnggota" class="form-control" type="text" value="" id="nama-anggota1" placeholder="NRP Anggota" required>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan-anggota" class="form-control-label">Jurusan anggota</label><br>
                                    <select name="jurusanAnggota" class="form-control" id="jurusanAnggota" required>
                                        <option value="" disabled selected>Jurusan</option>
                                        <?php  foreach ($rowJurusan as $hasilJurusan) {?>
                                            <option value="<?php echo $hasilJurusan['NOMOR']; ?>"><?php echo $hasilJurusan['JURUSAN_LENGKAP']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <?php
                        $id_tim = intval($rows[0]['ID_TIM']);
                        echo "<a class='btn btn-secondary' href='home.php?halaman=edit-data-tim&id=$id_tim' role='button'>Back</a>"
                        ?>
                    </form>
                    <?php
                    if (isset($_POST['save']))
                    {
                        $id = $_POST['timUMKM'];
                        $anggota = $_POST['nrpAnggota'];
                        $jurusan = $_POST['jurusanAnggota'];

                        $sql = "INSERT INTO ANGGOTA_TIM (ANGGOTA,ID_TIM,ID_JURUSAN) VALUES
                                        ('$anggota','$id','$jurusan')";
                        $hasil = query_insert($conn, $sql);

                        $id_tim = intval($rows[0]['ID_TIM']);
                        echo "<script>location='home.php?halaman=edit-data-tim&id=$id_tim';</script>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>