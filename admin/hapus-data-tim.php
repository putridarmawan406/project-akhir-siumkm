<?php

require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = "DELETE FROM TIM_KEWIRAUSAHAAN WHERE ID_TIM=:v1";
$hasil = query_delete($conn, $sql,$data);

echo "<script>alert('Data Berhasil Dihapus');</script>";
echo "<script>location='home.php?halaman=data-tim';</script>";
?>