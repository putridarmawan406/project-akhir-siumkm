<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM LAPORAN L 
        JOIN PEGAWAI P ON P.NOMOR=L.ID_DOSBIM   
        WHERE L.ID_LAPORAN = :v1 ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}

$sql2 = " SELECT * FROM STATUS_VALIDASI SV 
        JOIN LAPORAN L ON SV.ID_STATUS = L.STATUS_LAPORAN WHERE L.ID_LAPORAN = :v1";

$hasil2 = query_detail($conn, $sql2 , $data);
oci_fetch_all($hasil2, $rows2, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows2 as $hasil2) {
    $item2[] = $hasil2;
}
require_once "../API/dosenPembimbing.php";
$hasilDosbim = query_viewDosbim();
oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

include "../API/timKewirausahaan.php";
$hasilKWU = query_viewTimKewirausahaan();
oci_fetch_all($hasilKWU, $rowsKWU, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Detail Laporan</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Laporan</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Detail Data Laporan</h6>
                </div>
                <div class="container-fluid py-4">
                    <form enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <h6>Judul Laporan</h6>
                                    <h8><?php echo $hasil['JUDUL_LAPORAN']; ?></h8>
                                </div>
                                <div class="form-group">
                                    <h6>Dosen Pembimbing Tim UMKM</h6>
                                    <h8 name="dosbim" type="text" value="<?php echo $hasilDosbim['ID_DOSBIM']; ?>" id="dosbim"><?php echo $hasil['NAMA']; ?></h8>
                                </div>
                                <div class="form-group">
                                    <h6>Tanggal Upload</h6>
                                    <h8><?php echo date('d-M-Y',strtotime($hasil['TANGGAL_UPLOAD'])); ?></h8>
                                </div>
                            </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <h6>Catatan Dosen Pembimbing</h6>
                                        <h8><?php
                                            if($hasil['CATATAN_LAPORAN'] == NULL){
                                                echo "-";
                                            } else {
                                                echo "{$hasil['CATATAN_LAPORAN']}";
                                            }
                                            ?>
                                        </h8>
                                    </div>
                                    <div class="form-group">
                                        <h6>Status Laporan</h6>
                                        <h8><?php
                                            if($hasil['STATUS_LAPORAN'] == NULL){
                                                echo "-";
                                            } else {
                                                echo "{$hasil2['JENIS_STATUS']}";
                                            }
                                            ?>
                                        </h8>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h6>File Laporan</h6>
                                    <div class="mb-2 d-flex flex-column">
                                       <iframe src="../file_laporan/<?=$hasil['FILE_LAPORAN']?>" width="940" height="500"></iframe>
                                    </div>
                                </div>
                            </div>
                            <a class="btn btn-info" href="home.php?halaman=laporan" role="button">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
