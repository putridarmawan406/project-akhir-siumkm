<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM PENGUMUMAN P 
        WHERE P.ID_PENGUMUMAN = :v1 ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}

?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Edit Pengumuman</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Pengumuman</h6>
        </nav>
        <ul class="navbar-nav  justify-content-end">
            <div class="nav-item dropdown">
                <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user me-sm-1"></i>
                    <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                    if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                        echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                    }
                    ?>
                    <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                </ul>
            </div>
        </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Edit Pengumuman</h6>
                </div>
                <div class="container-fluid py-4">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="judul-pengumuman" class="form-control-label">Judul Pengumuman</label>
                            <input name="judulPengumuman" class="form-control" type="text" value="<?php echo $hasil['NAMA_PENGUMUMAN']; ?>" id="judul-pengumuman" placeholder="Judul Pengumuman" required>
                        </div>
                        <div class="mb-3">
                            <label for="formFile" class="form-label">File Pengumuman</label>
                            <br>
                            <iframe src="../file_pengumuman/<?=$hasil['FILE_PENGUMUMAN']?>" width="920" height="500"></iframe>
                            <input name="filePengumuman" class="form-control" type="file" value="<?php echo $hasil['FILE_PENGUMUMAN']; ?>" id="formFile">
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=data-pengumuman" role="button">Back</a>
                    </form>
                    <?php
                    if (isset($_POST['save']))
                    {
                        $name = $_POST['judulPengumuman'];
                        $ekstensi_diperbolehkan = array('pdf', '');
                        $nama = $_FILES['filePengumuman']['name'] == '' ? $hasil['FILE_PENGUMUMAN'] : $_FILES['filePengumuman']['name'];
                        $x = explode('.', $nama);
                        $ekstensi = strtolower(end($x));
                        $ukuran = $_FILES['filePengumuman']['size'];
                        $file_tmp = $_FILES['filePengumuman']['tmp_name'];

                        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
                            if ($ukuran < 8044070) {
                                move_uploaded_file($file_tmp, '../file_pengumuman/' . $nama);

                                $sql = "UPDATE PENGUMUMAN SET NAMA_PENGUMUMAN = '$name',FILE_PENGUMUMAN = '$nama' WHERE ID_PENGUMUMAN=:v1";
                                $hasil = query_update($conn, $sql, $data);
                                if ($hasil) {
                                    echo "<script>alert('Data Berhasil Disimpan');</script>";
                                    echo "<script>location='home.php?halaman=data-pengumuman';</script>";
                                } else {
                                    echo "<script>alert('Gagal Mengupload File');</script>";
                                }
                            } else {
                                echo "<script>alert('File terlalu besar');</script>";
                            }
                        } else {
                            echo "<script>alert('EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN');</script>";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
