<?php
        include "../API/logbook.php";


        if($_SESSION['StatusPengguna'] == "admin") {
            $hasil = query_viewLogbook();
            oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
        } elseif($_SESSION['StatusPengguna'] == "Pegawai") {
            $hasil = query_viewLogbookTimBimbingan();
            oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
            foreach ($rows as $hasil) {
                $item[] = $hasil;
            }
        } elseif($_SESSION['StatusPengguna'] == "Mahasiswa") {
            $hasil = query_viewLogbookTim();
            oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
            foreach ($rows as $hasil) {
                $item[] = $hasil;
            }
        }

        require_once "../API/status-validasi.php";
        $hasilStatus = query_viewStatus();
        oci_fetch_all($hasilStatus, $rowStatus, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
          <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                  <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                  </div>
              </a>
          </li>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Logbook</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Logbook</h6>
        </nav>
          <ul class="navbar-nav  justify-content-end">
              <div class="nav-item dropdown">
                  <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-user me-sm-1"></i>
                      <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php
                      if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                          echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                      }
                      ?>
                      <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                  </ul>
              </div>
          </ul>
      </div>
    </nav>
    <!-- End Navbar -->
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-md-12 mt-4">
          <div class="card">
            <div class="row">
            <div class="card-header pb-0 px-3">
              <h6 class="mb-0">Logbook UMKM</h6>
            </div>
            </div>
            <div class="ms-auto">
                <?php
                if($_SESSION['StatusPengguna'] == "Mahasiswa") {
                    echo "<a  class='btn btn-link text-info text-gradient px-3 mb-0' href = 'home.php?halaman=tambah-logbook'><i class='far fa-plus-square me-2'></i >Tambah Logbook</a >";
                }
                ?>
            </div>
            <div class="card-body pt-4 p-3">
              <div class="table-responsive px-3">
                <table class="table align-items-center mb-0" id="myTable">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Judul</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tanggal Uplaod</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  foreach ($rows as $hasil) {?>
                    <tr>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?php echo $hasil['JUDUL_LOGBOOK']; ?></h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <p class="text-xs font-weight-bold mb-0 text-center"><?php echo $hasil['TANGGAL_UPLOAD']; ?></p>
                      </td>
                        <td>
                            <p class="text-xs font-weight-bold mb-0 text-center" value=""><?php echo $hasil['JENIS_STATUS']; ?></p>
                        </td>
                      <td class="align-middle text-center">
                        <div class="ms-auto">
                          <a class="btn btn-link text-info text-gradient px-3 mb-0" href="home.php?halaman=detail-logbook&id=<?php echo $hasil['ID_LOGBOOK'];?>"><i class="far fa-eye me-2"></i>Detail</a>
                            <?php
                            if($_SESSION['StatusPengguna'] == "Mahasiswa") {
                                echo "<a  class='btn btn-link text-dark px-3 mb-0' href = 'home.php?halaman=edit-logbook&id={$hasil['ID_LOGBOOK']}'><i class='fas fa-pencil-alt text-dark me-2'></i > Edit</a >";
                                echo "<a  class='btn btn-link text-dark px-3 mb-0' href = 'home.php?halaman=hapus-logbook&id={$hasil['ID_LOGBOOK']}'><i class='fas fa-trash-alt text-red me-2'></i > Hapus</a >";
                            } elseif ($_SESSION['StatusPengguna'] == "Pegawai" ){
                                echo "<a  class='btn btn-link text-dark px-3 mb-0' href = 'home.php?halaman=validasi-logbook&id={$hasil['ID_LOGBOOK']}'><i class='fas fa-check-circle text-dark me-2'></i > Validasi</a >";
                            }
                            ?>
                            </div>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer pt-3  ">
        <div class="container-fluid">
          <div class="row align-items-center justify-content-lg-between">
              <div class="col-lg-8 mb-lg-0 mb-4">
                  <div class="copyright text-center text-sm text-muted text-lg-start">
                      © <script>
                          document.write(new Date().getFullYear())
                      </script>
                      Sistem Informasi UMKM Politeknik Elektronika Negeri Surabaya
                  </div>
              </div>
          </div>
        </div>
      </footer>
    </div>
  </main>