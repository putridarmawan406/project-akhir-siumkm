    <?php 
    require_once "../API/koneksi.php";
	  require "../includes/func.inc.php";
    ?>
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
          <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                  <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                  </div>
              </a>
          </li>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Kategori Pendanaan</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Tambah Kategori Pendanaan</h6>
        </nav>
            <ul class="navbar-nav  justify-content-end">
                <div class="nav-item dropdown">
                    <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user me-sm-1"></i>
                        <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?php
                        if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                            echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                        }
                        ?>
                        <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                    </ul>
                </div>
            </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Tambah Kategori UMKM</h6>
            </div>
                    <div class="container-fluid py-4">
                    <form method="POST">
                        <div class="form-group">
                            <label for="kategori-UMKM" class="form-control-label">Kategori Pendanaan</label>
                            <input name="kategoriUMKM" class="form-control" type="text" value="" id="kategori-UMKM" placeholder="Kategori UMKM" required>
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                        <a class="btn btn-secondary" href="home.php?halaman=kategori" role="button">Back</a>
                        </form>
                        <?php 
                        if (isset($_POST['save']))
                        {
                          $nama = $_POST['kategoriUMKM'];
                          $sql = "INSERT INTO KATEGORI_UMKM (ID_KATEGORI,NAMA_KATEGORI) VALUES (kategori_seq.NEXTVAL,'$nama')";
                          $hasil = query_insert($conn, $sql);

                          // echo "<script>alert('Data Berhasil Disimpan');</script>";
	                        echo "<script>location='home.php?halaman=kategori';</script>";
                        }
                        ?>
                </div>
            </div>
        </div>
        </div>
    </div>