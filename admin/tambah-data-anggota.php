<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";

require_once "../API/dosenPembimbing.php";
$hasilDosbim = query_viewDosbim();
oci_fetch_all($hasilDosbim, $rowDosbim, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

require_once "../API/jurusan.php";
$hasilJurusan = query_viewJurusan();
oci_fetch_all($hasilJurusan, $rowJurusan, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </a>
        </li>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Data Anggota Tim</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Anggota Tim</h6>
        </nav>
            <ul class="navbar-nav  justify-content-end">
                <div class="nav-item dropdown">
                    <button class="btn bg-gradient-primary dropdown-toggle mb-0" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user me-sm-1"></i>
                        <span class="d-sm-inline d-none"><?php echo $_SESSION['Nama'] ?></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?php
                        if($_SESSION['StatusPengguna'] == "Mahasiswa" OR $_SESSION['StatusPengguna'] == "Pegawai") {
                            echo "<li><a class='dropdown-item' href='home.php?halaman=profil'>Profil</a></li>";
                        }
                        ?>
                        <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                    </ul>
                </div>
            </ul>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Tambah Data Anggota Tim UMKM</h6>
                </div>
                <div class="container-fluid py-4">
                    <form method="POST">
                        <div class="row">
                            <div class="form-group">
                                <label for="timUMKM" class="form-control-label">Nama Tim UMKM</label>
                                <?php
                                $id = $_GET['id'];
                                //                                    print_r($id);
                                $sql = "SELECT * FROM TIM_KEWIRAUSAHAAN WHERE ID_TIM = :v1";
                                $data = array(':v1' => $id);
                                $hasil = query_view($conn, $sql, $data);

                                oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
                                ?>
                                <input name="timUMKM" class="form-control" type="hidden" value="<?= $rows[0]['ID_TIM'] ?>" id="timUMKM" placeholder="tim UMKM">
                                <input name="" class="form-control" type="text" value="<?= $rows[0]['NAMA_TIM'] ?>" id="timUMKM" placeholder="tim UMKM" disabled>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nama-anggota1" class="form-control-label">NRP Anggota 1</label>
                                    <input name="nrpAnggota1" class="form-control" type="text" value="" id="nama-anggota1" placeholder="NRP Anggota 1" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama-anggota2" class="form-control-label">Nama Anggota 2</label>
                                    <input name="nrpAnggota2" class="form-control" type="text" value="" id="nama-anggota2" placeholder="NRP Anggota 2" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama-anggota3" class="form-control-label">NRP Anggota 3</label>
                                    <input name="nrpAnggota3" class="form-control" type="text" value="" id="nama-anggota3" placeholder="NRP Anggota 3" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama-anggota4" class="form-control-label">NRP Anggota 4</label>
                                    <input name="nrpAnggota4" class="form-control" type="text" value="" id="nama-anggota4" placeholder="NRP Anggota 4">
                                </div>
                                <div class="form-group">
                                    <label for="nrpAnggota5" class="form-control-label">NRP Anggota 5</label>
                                    <input name="nrpAnggota5" class="form-control" type="text" value="" id="nrpAnggota5" placeholder="NRP Anggota 5">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="jurusan-anggota1" class="form-control-label">Jurusan anggota1</label><br>
                                    <select name="jurusanAnggota1" class="form-control" id="jurusanAnggota1" required>
                                        <option value="" disabled selected>Jurusan</option>
                                        <?php  foreach ($rowJurusan as $hasilJurusan) {?>
                                            <option value="<?php echo $hasilJurusan['NOMOR']; ?>"><?php echo $hasilJurusan['JURUSAN_LENGKAP']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan-Anggota2" class="form-control-label">Jurusan Anggota2</label><br>
                                    <select name="jurusanAnggota2" class="form-control" id="jurusanAnggota2" required>
                                        <option value="" disabled selected>Jurusan</option>
                                        <?php  foreach ($rowJurusan as $hasilJurusan) {?>
                                            <option value="<?php echo $hasilJurusan['NOMOR']; ?>"><?php echo $hasilJurusan['JURUSAN_LENGKAP']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan-Anggota3" class="form-control-label">Jurusan Anggota3</label><br>
                                    <select name="jurusanAnggota3" class="form-control" id="jurusanAnggota3" required>
                                        <option value="" disabled selected>Jurusan</option>
                                        <?php  foreach ($rowJurusan as $hasilJurusan) {?>
                                            <option value="<?php echo $hasilJurusan['NOMOR']; ?>"><?php echo $hasilJurusan['JURUSAN_LENGKAP']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan-Anggota4" class="form-control-label">Jurusan Anggota4</label><br>
                                    <select name="jurusanAnggota4" class="form-control" id="jurusanAnggota4" >
                                        <option value="" disabled selected>Jurusan</option>
                                        <?php  foreach ($rowJurusan as $hasilJurusan) {?>
                                            <option value="<?php echo $hasilJurusan['NOMOR']; ?>"><?php echo $hasilJurusan['JURUSAN_LENGKAP']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan-Anggota5" class="form-control-label">Jurusan Anggota5</label><br>
                                    <select name="jurusanAnggota5" class="form-control" id="jurusanAnggota5" >
                                        <option value="" disabled selected>Jurusan</option>
                                        <?php  foreach ($rowJurusan as $hasilJurusan) {?>
                                            <option value="<?php echo $hasilJurusan['NOMOR']; ?>"><?php echo $hasilJurusan['JURUSAN_LENGKAP']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button name="save" type="submit" class="btn btn-info">Submit</button>
                    </form>
                    <?php
                    if (isset($_POST['save']))
                    {
                          $id = $_POST['timUMKM'];
                          if($_POST['nrpAnggota1'] && $_POST['jurusanAnggota1']){
                              $anggota1 = $_POST['nrpAnggota1'];
                              $jurusan1 = $_POST['jurusanAnggota1'];

                              $sql1 = "INSERT INTO ANGGOTA_TIM (ANGGOTA,ID_TIM,ID_JURUSAN) VALUES
                                    ('$anggota1','$id','$jurusan1')";
                              $hasil1 = query_insert($conn, $sql1);
                          }

                          if($_POST['nrpAnggota2'] && $_POST['jurusanAnggota2']){
                            $anggota2 = $_POST['nrpAnggota2'];
                            $jurusan2 = $_POST['jurusanAnggota2'];

                            $sql2 = "INSERT INTO ANGGOTA_TIM (ANGGOTA,ID_TIM,ID_JURUSAN) VALUES
                                        ('$anggota2','$id','$jurusan2')";
                            $hasil2 = query_insert($conn, $sql2);
                          }

                          if($_POST['nrpAnggota3'] && $_POST['jurusanAnggota3']){
                            $anggota3 = $_POST['nrpAnggota3'];
                            $jurusan3 = $_POST['jurusanAnggota3'];

                            $sql3 = "INSERT INTO ANGGOTA_TIM (ANGGOTA,ID_TIM,ID_JURUSAN) VALUES
                                        ('$anggota3','$id','$jurusan3')";
                            $hasil3 = query_insert($conn, $sql3);
                          }

                          if($_POST['nrpAnggota4'] && $_POST['jurusanAnggota4']){
                            $anggota4 = $_POST['nrpAnggota4'];
                            $jurusan4 = $_POST['jurusanAnggota4'];

                            $sql = "INSERT INTO ANGGOTA_TIM (ANGGOTA,ID_TIM,ID_JURUSAN) VALUES
                                        ('$anggota4','$id','$jurusan4')";
                            $hasil = query_insert($conn, $sql);
                        }

                        if($_POST['nrpAnggota5'] && $_POST['jurusanAnggota5']){
                            $anggota5 = $_POST['nrpAnggota5'];
                            $jurusan5 = $_POST['jurusanAnggota5'];

                            $sql = "INSERT INTO ANGGOTA_TIM (ANGGOTA,ID_TIM,ID_JURUSAN) VALUES
                                        ('$anggota5','$id','$jurusan5')";
                            $hasil = query_insert($conn, $sql);
                        }
	                        echo "<script>location='home.php?halaman=data-tim';</script>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>