<?php
require_once "../API/koneksi.php";
require "../includes/func.inc.php";
$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM LOMBA L WHERE L.ID_LOMBA = :v1 ";

$hasil = query_detail($conn, $sql , $data);
oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

foreach ($rows as $hasil) {
    $item[] = $hasil;
}
?>
<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Detail Lomba</li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Lomba</h6>
        </nav>
    </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Detail Data Lomba</h6>
                </div>
                <div class="container-fluid py-4">
                    <form>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <h6>Nama Lomba</h6>
                                    <h8><?php echo $hasil['NAMA_LOMBA']; ?></h8>
                                </div>
                                <div class="form-group">
                                    <h6>Waktu Pelaksanaan Lomba</h6>
                                    <h8><?php echo date('d-M-Y',strtotime($hasil['WAKTU_LOMBA'])); ?></h8>
                                </div>
                                <div class="form-group">
                                    <h6>Link Pendaftaran Lomba</h6>
                                    <h8><?php echo $hasil['LINK_LOMBA']; ?></h8>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <h6>Poster Lomba</h6>
                                    <div class="mb-2 d-flex flex-column">
                                        <!--                           <span class="mb-2" name="old" value="--><?//=$hasil['POSTER_LOMBA']?><!--">--><?php //echo $hasil['POSTER_LOMBA'];?><!--</span>-->
                                        <img src="../foto_lomba/<?=$hasil['POSTER_LOMBA']?>" alt="" style="max-width: 350px;">
                                    </div>
                                </div>
                            </div>
                            <a class="btn btn-info" href="home.php?halaman=dashboard" role="button">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>