<?php

require_once "../API/koneksi.php";
require "../includes/func.inc.php";

$nomor = $_GET['id'];

$data = array(
    ':v1' => $nomor
);

$sql = " SELECT * FROM PENGUMUMAN P 
        WHERE P.ID_PENGUMUMAN = :v1 ";

$hasilPengumuman = query_detail($conn, $sql , $data);
oci_fetch_all($hasilPengumuman, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

$filePengumuman=$rows[0]['FILE_PENGUMUMAN'];

if(file_exists("../file_pengumuman/$filePengumuman")){
    unlink("../file_pengumuman/$filePengumuman");
}

$sql2 = "DELETE FROM PENGUMUMAN WHERE ID_PENGUMUMAN=:v1";
$hasil = query_delete($conn, $sql2,$data);

echo "<script>alert('Data berhasil dihapus');</script>";
echo "<script>location='home.php?halaman=data-pengumuman';</script>";
?>

