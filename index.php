<?php include "layout/headerIndex.php"; ?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sistem Informasi UMKM Mahasiswa PENS</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/contents/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/contents/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="contents/css/jquery.fancybox.min.css"/>
<link rel="stylesheet" type="text/css" href="contents/css/owl.carousel.min.css"/>
<link rel="stylesheet" type="text/css" href="contents/css/owl.theme.default.min.css"/>

<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg">
  <div class="container"> <a class="navbar-brand navbar-logo" href="#"> <img src="contents/images/logo-umkm22.png" alt="logo" class="logo-1" style="width: 180px"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="fas fa-bars"></span> </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active"> <a class="nav-link" href="#" data-scroll-nav="1">About</a> </li>
        <li class="nav-item active"> <a class="nav-link" href="#" data-scroll-nav="2">Testimoni</a> </li>
<!--        <li class="nav-item active"> <a class="navbar-btn" href="#" data-scroll-nav="">Login</a> </li>-->
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Login</a>
              <div class="dropdown-menu">
                  <a class="dropdown-item" href="loginMahasiswa.php">Mahasiswa</a>
                  <a class="dropdown-item" href="loginPegawai.php">Pegawai</a>
<!--                  <a class="dropdown-item" href="admin/index.php">Admin</a>-->
              </div>
          </li>
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar --> 

<!-- Banner Image -->

<div class="banner text-center" data-scroll-index='0'>
  <div class="banner-overlay">
    <div class="container">
      <h1 class="text-capitalize">Temukan Informasi Kompetisi dan Pelatihan Kewirausahaan</h1>
      <a href="webinar.php" class="banner-btn">Webinar</a>
      <a href="kompetisi.php" class="banner-btn">Kompetisi</a>
      <a href="informasi.php" class="banner-btn">Informasi</a>
    </div>
  </div>
</div>

<!-- End Banner Image --> 

<!-- About -->

<div class="about-us section-padding" data-scroll-index='1'>
  <div class="container">
    <div class="row">
      <div class="col-md-12 section-title text-center">
        <h3>Kewirausahaan Politeknik Eletronika Negeri Surabaya</h3>
        <span class="section-title-line"></span> </div>
      <div class="col-md-6 mb-50">
        <div class="section-info">
          <div class="sub-title-paragraph">
            <h4>UMKM PENS</h4>
            <h5>Menumbuhkan jiwa kewirausahaan Mahasiswa PENS</h5>
            <p>UMKM PENS adalah adalah platform yang memberikan informasi lengkap seputar UMKM mahasiswa Politeknik Elektronika Negeri Surabaya, dengan banyak fitur dan mudah digunakan serta memiliki pengalaman pengguna yang baik. Dengan desain yang menarik sehingga membuat user nyaman untuk mengakses.</p>
          </div>
          <a href="https://kemahasiswaan.pens.ac.id/pmw/" class="anchor-btn">Learn more <i class="fas fa-arrow-right pd-lt-10"></i></a> </div>
      </div>
      <div class="col-md-6 mb-50">
        <div class="section-img"> <img src="contents/images/fix2.jpg" alt="" class="img-responsive"/> </div>
      </div>
    </div>
  </div>
</div>

<!-- End About -->

<!-- Testimonials -->
<div class="testimonials">
    <div class="testimonials-overlay section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="owl-carousel owl-theme">
                        <div class="testimonial-item text-center">
                            <div class="icon"> <i class="fas fa-comments"></i> </div>
                            <p class="m-auto">Politeknik Elektronika Negeri Surabaya (PENS) bekerjasama dengan beberapa Usaha Kecil dan Menengah (UKM) yang tergabung dalam HIPMI untuk proses bimbingan praktis wirausaha mulai dari seminar, diklat, kunjungan, penyusunan rencana bisnis, dan pendampingan.</p>
                            <div class="testimonial-author text-center">
                                <h5>SI UMKM</h5>
                                <h6>~</h6>
                            </div>
                        </div>
                        <div class="testimonial-item text-center">
                            <div class="icon"> <i class="fas fa-comments"></i> </div>
                            <p class="m-auto">Kegiatan Bidang Wirausaha berupa Persiapan Program Mahasiswa Wirausaha seperti : Sosialisasi PMW, Seleksi Proposal Ide Bisnis, Simulasi Wirausaha, Pemberian Bantuan Modal Usaha</p>
                            <div class="testimonial-author text-center">
                                <h5>SI UMKM</h5>
                                <h6>~</h6>
                            </div>
                        </div>
                        <div class="testimonial-item text-center">
                            <div class="icon"> <i class="fas fa-comments"></i> </div>
                            <p class="m-auto">Kegiatan Bidang Wirausaha yang lain berupa Pendidikan dan Pelatihan Kewirausahaan seperti : Motivasi Wirausaha, Diklat Wirausaha, Kunjungan di UKM, Magang di UKM dan Ekspo PMW Nasional.</p>
                            <div class="testimonial-author text-center">
                                <h5>SI UMKM</h5>
                                <h6>~</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Testimonials -->

<!-- Services -->
<div class="services section-padding bg-grey" data-scroll-index='2'>
  <div class="container">
    <div class="row">
      <div class="col-md-12 section-title text-center">
        <h3>Informasi Lebih Lengkap Seputar Produk PENS Preneur</h3>
        <p><a href="https://kewirausahaan.pens.ac.id/" class="banner-btn">Website Pens Preneur</a></p>
        <span class="section-title-line"></span> </div>
<!--      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-30">-->
<!--        <div class="service-box bg-white text-center">-->
<!--          <div class="icon"> <i class="fas fa-folder"></i> </div>-->
<!--          <div class="icon-text">-->
<!--            <h4 class="title-box">Penyimpanan File</h4>-->
<!--            <p>Sed malesuada, est eget condimentum iaculis, nisi ex facilisis metus.</p>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-30">-->
<!--        <div class="service-box bg-white text-center">-->
<!--          <div class="icon"> <i class="fas fa-bullhorn "></i> </div>-->
<!--          <div class="icon-text">-->
<!--            <h4 class="title-box">Informasi Akurat</h4>-->
<!--            <p>Sed malesuada, est eget condimentum iaculis, nisi ex facilisis metus.</p>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-30">-->
<!--        <div class="service-box bg-white text-center">-->
<!--          <div class="icon"> <i class="fas fa-map-marked"></i> </div>-->
<!--          <div class="icon-text">-->
<!--            <h4 class="title-box">Mark Location</h4>-->
<!--            <p>Sed malesuada, est eget condimentum iaculis, nisi ex facilisis metus.</p>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-30">-->
<!--        <div class="service-box bg-white text-center">-->
<!--          <div class="icon"> <i class="fas fa-bug"></i> </div>-->
<!--          <div class="icon-text">-->
<!--            <h4 class="title-box">Bug Solution</h4>-->
<!--            <p>Sed malesuada, est eget condimentum iaculis, nisi ex facilisis metus.</p>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-30">-->
<!--        <div class="service-box bg-white text-center">-->
<!--          <div class="icon"> <i class="fas fa-comments"></i> </div>-->
<!--          <div class="icon-text">-->
<!--            <h4 class="title-box">Fast Communication</h4>-->
<!--            <p>Sed malesuada, est eget condimentum iaculis, nisi ex facilisis metus.</p>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-30">-->
<!--        <div class="service-box bg-white text-center">-->
<!--          <div class="icon"> <i class="fas fa-paint-brush"></i> </div>-->
<!--          <div class="icon-text">-->
<!--            <h4 class="title-box">Clean Design</h4>-->
<!--            <p>Sed malesuada, est eget condimentum iaculis, nisi ex facilisis metus.</p>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
    </div>
  </div>
</div>

<!-- End Services --> 

<!-- Gallery -->
<!-- <div class="portfolio section-padding" data-scroll-index='3'>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 section-title text-center">
        <h3>Check our recent works</h3>
        <p>Vestibulum elementum dui tempus dolor gravida, vel mattis erat fermentum.</p>
        <span class="section-title-line"></span> </div>
      <div class="filtering text-center mb-30">
        <button type="button" data-filter='*' class="active">All</button>
        <button type="button" data-filter='.summer'>Summer</button>
        <button type="button" data-filter='.winter'>Winter</button>
        <button type="button" data-filter='.rainy'>Rainy</button>
        <button type="button" data-filter='.spring'>Spring</button>
      </div>
      <div class="gallery no-padding col-lg-12 col-sm-12">
        <div class="col-lg-4 col-sm-6 spring no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall1.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall1.jpg" alt="image">
              <div class="overlay-img">
                <h4>Spring</h4>
                <h6>Autumn Leaves</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 spring no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall2.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall2.jpg" alt="image">
              <div class="overlay-img">
                <h4>Spring</h4>
                <h6>Flower Pot</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 spring no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall3.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall3.jpg" alt="image">
              <div class="overlay-img">
                <h4>Spring</h4>
                <h6>Bird with Flower</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 summer no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall4.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall4.jpg" alt="image">
              <div class="overlay-img">
                <h4>Summer</h4>
                <h6>Vacation Trip Van</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 winter no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall5.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall5.jpg" alt="image">
              <div class="overlay-img">
                <h4>Winter</h4>
                <h6>Girl in the snow</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 rainy no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall6.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall6.jpg" alt="image">
              <div class="overlay-img">
                <h4>Rainy</h4>
                <h6>Man with an Umberalla</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 summer no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall7.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall7.jpg" alt="image">
              <div class="overlay-img">
                <h4>Summer</h4>
                <h6>New Beginning</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 rainy no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall8.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall8.jpg" alt="image">
              <div class="overlay-img">
                <h4>Rainy</h4>
                <h6>Girl in the Rain</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 summer no-padding">
          <div class="item-img"> <a class="single-image" href="contents/images/gall9.jpg"></a>
            <div class="part-img"> <img src="contents/images/gall9.jpg" alt="image">
              <div class="overlay-img">
                <h4>Summer</h4>
                <h6>Fun at the Seashores</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- End Gallery -->

<!-- Contact -->
<!--<div class="contact section-padding" data-scroll-index='4'>-->
<!--  <div class="container">-->
<!--    <div class="row">-->
<!--      <div class="col-md-12 section-title text-center">-->
<!--        <h3>Contact Us For More</h3>-->
<!--        <p>Vestibulum elementum dui tempus dolor gravida, vel mattis erat fermentum.</p>-->
<!--        <span class="section-title-line"></span> </div>-->
<!--      <div class="col-lg-5 col-md-4">-->
<!--        <div class="part-info">-->
<!--          <div class="info-box">-->
<!--            <div class="icon"> <i class="fas fa-phone"></i> </div>-->
<!--            <div class="content">-->
<!--              <h4>Phone :</h4>-->
<!--              <p>0123456789</p>-->
<!--            </div>-->
<!--          </div>-->
<!--          <div class="info-box">-->
<!--            <div class="icon"> <i class="fas fa-map-marker-alt"></i> </div>-->
<!--            <div class="content">-->
<!--              <h4>Address :</h4>-->
<!--              <p>New Delhi, India</p>-->
<!--            </div>-->
<!--          </div>-->
<!--          <div class="info-box">-->
<!--            <div class="icon"> <i class="fas fa-envelope"></i> </div>-->
<!--            <div class="content">-->
<!--              <h4>Mail :</h4>-->
<!--              <p><a href="#">info@123.com</a></p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="col-lg-7 col-md-8">-->
<!--        <div class="contact-form">-->
<!--          <form class='form' id='contact-form' method='post' data-toggle='validator'>-->
<!--            <input type='hidden' name='form-name' value='contact-form' />-->
<!--            <div class="messages"></div>-->
<!--            <div class="controls">-->
<!--              <div class="row">-->
<!--                <div class="col-lg-6">-->
<!--                  <div class="form-group">-->
<!--                    <input id="form_name" type="text" name="name" placeholder="Name *" required data-error="name is required.">-->
<!--                    <div class="help-block with-errors"></div>-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="col-lg-6">-->
<!--                  <div class="form-group">-->
<!--                    <input id="form_email" type="email" name="email" placeholder="Email *" required data-error="Valid email is required.">-->
<!--                    <div class="help-block with-errors"></div>-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="col-lg-12">-->
<!--                  <div class="form-group">-->
<!--                    <input id="form_subject" type="text" name="subject" placeholder="Subject">-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="col-lg-12 form-group">-->
<!--                  <textarea id="form_message" name="message" class="form-control" placeholder=" Type Your Message " rows="4" required data-error="Please,leave us a message."></textarea>-->
<!--                  <div class="help-block with-errors"></div>-->
<!--                </div>-->
<!--                <div class="col-lg-12 text-center">-->
<!--                  <button class="bttn">Send Message</button>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--          </form>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</div>-->
<!-- End Contact -->
<footer class="footer-copy">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <p>&copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            Sistem Informasi UMKM Politeknik Elektronika Negeri Surabaya
        </p>
      </div>
    </div>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script> 
<!-- owl carousel js --> 
<script src="contents/js/owl.carousel.min.js"></script> 
<!-- magnific-popup --> 
<script src="contents/js/jquery.fancybox.min.js"></script> 

<!-- scrollIt js --> 
<script src="contents/js/scrollIt.min.js"></script> 

<!-- isotope.pkgd.min js --> 
<script src="contents/js/isotope.pkgd.min.js"></script> 
<script>
  $(window).on("scroll",function () {

      var bodyScroll = $(window).scrollTop(),
          navbar = $(".navbar");

      if(bodyScroll > 130){

          navbar.addClass("nav-scroll");
          $('.navbar-logo img').attr('src','contents/images/logo-umkm22.png');


      }else{

          navbar.removeClass("nav-scroll");
          $('.navbar-logo img').attr('src','contents/images/logo-umkm22.png');

      }

  });

  $(window).on("load",function (){



var bodyScroll = $(window).scrollTop(),
navbar = $(".navbar");

if(bodyScroll > 130){

navbar.addClass("nav-scroll");
$('.navbar-logo img').attr('src','contents/images/logo-black.png');


}else{

navbar.removeClass("nav-scroll");
$('.navbar-logo img').attr('src','contents/images/logo-white.png');

}

/* smooth scroll
  -------------------------------------------------------*/
  $.scrollIt({

easing: 'swing',      // the easing function for animation
scrollTime: 900,       // how long (in ms) the animation takes
activeClass: 'active', // class given to the active nav element
onPageChange: null,    // function(pageIndex) that is called when page is changed
topOffset: -63
});

/* isotope
-------------------------------------------------------*/
var $gallery = $('.gallery').isotope({});
$('.gallery').isotope({

    // options
    itemSelector: '.item-img',
    transitionDuration: '0.5s',

});


$(".gallery .single-image").fancybox({
  'transitionIn'  : 'elastic',
  'transitionOut' : 'elastic',
  'speedIn'   : 600,
  'speedOut'    : 200,
  'overlayShow' : false
});


/* filter items on button click
-------------------------------------------------------*/
$('.filtering').on( 'click', 'button', function() {

    var filterValue = $(this).attr('data-filter');

    $gallery.isotope({ filter: filterValue });

    });

$('.filtering').on( 'click', 'button', function() {

    $(this).addClass('active').siblings().removeClass('active');

});

})

$(function () {
$( ".cover-bg" ).each(function() {
    var attr = $(this).attr('data-image-src');

    if (typeof attr !== typeof undefined && attr !== false) {
      $(this).css('background-image', 'url('+attr+')');
    }

  });

  /* sections background color from data background
  -------------------------------------------------------*/
  $("[data-background-color]").each(function() {
      $(this).css("background-color", $(this).attr("data-background-color")  );
  });


/* Owl Caroursel testimonial
  -------------------------------------------------------*/
  
  $('.testimonials .owl-carousel').owlCarousel({
    loop:true,
    autoplay:true,
    items:1,
    margin:30,
    dots: true,
    nav: false,
    
  });

});

</script>
</body>
</html>
