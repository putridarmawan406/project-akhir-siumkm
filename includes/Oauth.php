<?php
require "func.inc.php";

class Oauth {

    public function login($email, $password) {
        //  koneksi
        $conn=oci_connect('PA0040','381123','10.252.209.213/orcl.mis.pens.ac.id');

        $header=array("netid: $email","password: ".base64_encode($password));
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($data, CURLOPT_HTTPHEADER, $header);
        curl_setopt($data, CURLOPT_URL, "https://login.pens.ac.id/auth/");
        curl_setopt($data, CURLOPT_TIMEOUT, 9);

        $hasil = curl_exec($data);
        $dataDcode=json_decode($hasil);
        // $x =substr($dataDcode,0,5);

        if($hasil == "auth error"){
            echo "<script>alert('Login Gagal');</script>";
        }
        else{
            //    session_start();

            // var_dump($dataDcode->NRP);

            if(isset($dataDcode->NRP)) {
                echo "<script>alert('Mahasiswa Tidak Boleh Login');</script>";
                echo "<script>window.location.href='index.php';</script>";
            }
            elseif ($dataDcode->Name == "EEPIS Network Administrator"){
                    $_SESSION['StatusPengguna'] = "admin";

                $nip = $dataDcode->NIP;
                $sql = " SELECT P.NOMOR FROM PEGAWAI P WHERE P.NIP = '$nip' ";

                $hasil = query_getAll($conn, $sql);
                oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

//                $_SESSION['admin'] = $rows[0]['NOMOR'];
                $_SESSION['Nama'] = $dataDcode->Name;
                $_SESSION['Netid'] = $dataDcode->netid;
                $_SESSION['Group'] = $dataDcode->Group;
                $_SESSION['login'] = true;
                } else {
                $nip = $dataDcode->NIP;
                $sql = " SELECT P.NOMOR FROM PEGAWAI P WHERE P.NIP = '$nip' ";

                $hasil = query_getAll($conn, $sql);
                oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

                $idDosen = $rows[0]['NOMOR'];
                $_SESSION['dosbim'] = $idDosen;
                $_SESSION['StatusPengguna'] = "Pegawai";
                $_SESSION['Nama'] = $dataDcode->Name;
                $_SESSION['Netid'] = $dataDcode->netid;
                $_SESSION['Group'] = $dataDcode->Group;
                $_SESSION['login'] = true;
            }


            // var_dump($dataDcode);
            echo "<script>alert('Login Berhasil');</script>";
            echo "<script>window.location.href='admin/home.php?halaman=dashboard';</script>";

        }
        curl_close($data);

    }
    public function logout() {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($data, CURLOPT_URL, "https://login.pens.ac.id/auth/logout");
        curl_setopt($data, CURLOPT_TIMEOUT, 9);

        $hasil = curl_exec($data);
        curl_close($data);
    }
}
