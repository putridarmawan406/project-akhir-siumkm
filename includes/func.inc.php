<?php
//  koneksi
$conn=oci_connect('PA0040','381123','10.252.209.213/orcl.mis.pens.ac.id');

/**
 * eksekusi query db
 * untuk memudahkan penulisan saja 
 * karena oracle membutuhkan 
 * beberapa langkah
 * 
 * @param  oci_resource $con variable koneksi oci
 * @param  string $sql query yang di jalankan
 * @return oci_resource      resouce oci
//  */
function query_getAll($con, $sql)
{
	$parse = oci_parse($con, $sql);

	oci_execute($parse);
    return $parse;
}

function query_view($con, $sql, $data)
{
    $parse = oci_parse($con, $sql);
	foreach ($data as $key => $val) {    
    	oci_bind_by_name($parse, $key, $data[$key]);
	}
    oci_execute($parse);
    return $parse;
}

function query_detail($con, $sql, $data)
{
    $parse = oci_parse($con, $sql);
	foreach ($data as $key => $val) {    
    	oci_bind_by_name($parse, $key, $data[$key]);
	}
    oci_execute($parse);
    return $parse;
}

function query_count($con, $sql)
{
    $parse = oci_parse($con, $sql);
    oci_execute($parse);
    return $parse;
}

function query_insert($con, $sql)
{
    $parse = oci_parse($con, $sql);
	
    oci_execute($parse);
	if (oci_num_rows($parse)>0)
    	return "Success Insert";
	else
		return "Failed Insert";	
}

function query_update($con, $sql, $data)
{
    $parse = oci_parse($con, $sql);
	foreach ($data as $key => $val) {    
    	oci_bind_by_name($parse, $key, $data[$key]);
	}
    oci_execute($parse);
	if (oci_num_rows($parse)>0)
    	return "Success Update";
	else
		return "Failed Update";	
}

function query_delete($con, $sql, $data)
{
    $parse = oci_parse($con, $sql);
	foreach ($data as $key => $val) {    
    	oci_bind_by_name($parse, $key, $data[$key]);
	}
    oci_execute($parse);
	if (oci_num_rows($parse)>0)
    	return "Success Delete";
	else
		return "Failed Delete";	
}
?>