<?php
require_once "includes/Oauth.php";
session_start();
$obj = new Oauth();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $email = $_POST['email'];
    $password = $_POST['password'];
    $obj->login($email, $password);
}
//var_dump($_SESSION);
include "layout/headerIndex.php";
?>

<body>
  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
          <div class="container-fluid">
            <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 " href="../pages/dashboard.html">
              SI UMKM PENS 
            </a>
            <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon mt-2">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </span>
            </button>
<!--            <div class="collapse navbar-collapse" id="navigation">-->
<!--              <ul class="navbar-nav mx-auto">-->
<!--                <li class="nav-item">-->
<!--                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="admin/home.php?halaman=dashboard">-->
<!--                    <i class="fa fa-chart-pie opacity-6 text-dark me-1"></i>-->
<!--                    Dashboard-->
<!--                  </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                  <a class="nav-link me-2" href="admin/home.php?halaman=profile">-->
<!--                    <i class="fa fa-user opacity-6 text-dark me-1"></i>-->
<!--                    Profile-->
<!--                  </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                  <a class="nav-link me-2" href="sign-up.php">-->
<!--                    <i class="fas fa-user-circle opacity-6 text-dark me-1"></i>-->
<!--                    Sign Up-->
<!--                  </a>-->
<!--                </li>-->
<!--              </ul>-->
<!--            </div>-->
<!--          </div>-->
        </nav>
        <!-- End Navbar -->
      </div>
    </div>
  </div>
  <main class="main-content  mt-0">
    <section>
      <div class="min-vh-75">
        <div class="container">
          <div class="row">
            <div class="col-6 d-flex flex-column mx-auto">
              <div class="card-plain mt-8 p-3" style="border-radius: 20px; border: 2px solid aliceblue;">
                <div class="card-header pb-0 text-left bg-transparent">
                  <h3 class="font-weight-bolder text-info text-gradient">Login Pegawai</h3>
                  <p class="mb-0">Enter your email and password to sign in</p>
                </div>
                <div class="card-body">
                  <form role="form" method="post">
                    <label>Email</label>
                    <div class="mb-3">
                      <input name="email" type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="email-addon">
                    </div>
                    <label>Password</label>
                    <div class="mb-3">
                      <input name="password" type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="password-addon">
                    </div>
                    <div class="text-center">
<!--                      <button type="button" class="btn bg-gradient-info w-100 mt-4 mb-0" name="login">Sign in</button>-->
                        <button class="btn btn-primary btn bg-gradient-info w-100 mt-4 mb-0" type="submit" name="login">Login</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('contents/images/kew.png')"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!--   Core JS Files   -->
  <script src="contents/js/core/popper.min.js"></script>
  <script src="contents/js/core/bootstrap.min.js"></script>
  <script src="contents/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="contents/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="contents/js/soft-ui-dashboard.min.js?v=1.0.3"></script>
</body>

</html>