<?php include "layout/headerIndex.php"; ?>
<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
<?php
if(isset($_GET['halaman'])) {
    if ($_GET['halaman'] == "detail-pengumuman-index") {
        include 'detail-pengumuman-index.php';
    } else {
        include 'index.php';
    }
}
?>
<?php
include "layout/footerIndex.php";
require "includes/config.inc.php";
?>